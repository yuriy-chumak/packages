# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=nspr
pkgver=4.35
pkgrel=0
pkgdesc="Netscape Portable Runtime"
url="https://firefox-source-docs.mozilla.org/nspr/index.html"
arch="all"
options="!check"  # No test suite.
license="MPL-1.1 AND GPL-2.0-only AND LGPL-2.1-only"
depends=""
# -dev package does not ship any symlinks so dependency cannot be autodetected
depends_dev="nspr"
makedepends="autoconf automake"
subpackages="$pkgname-dev"
source="https://ftp.mozilla.org/pub/mozilla.org/$pkgname/releases/v$pkgver/src/$pkgname-$pkgver.tar.gz
	stacksize.patch
	"

prepare() {
	mkdir build inst
	default_prepare
}

build() {
	case "$CARCH" in
		*64* | s390x) _conf="--enable-64bit";;
	esac
	cd "$builddir"/build
	# ./nspr/pr/include/md/_linux.h tests only __GLIBC__ version
	# to detect c-library features, list musl features here for now.
	CFLAGS="$CFLAGS -D_PR_POLL_AVAILABLE -D_PR_HAVE_OFF64_T -D_PR_INET6 -D_PR_HAVE_INET_NTOP -D_PR_HAVE_GETHOSTBYNAME2 -D_PR_HAVE_GETADDRINFO -D_PR_INET6_PROBE" \
	../nspr/configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-debug \
		--enable-optimize \
		--enable-ipv6 \
		$_conf
	make CC="${CC:-gcc}" CXX="${CXX:-g++}"
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install

	cd "$pkgdir"/usr/lib
	rm -f *.a

	cd "$builddir"/build/config
	install -Dm755 nspr-config "$pkgdir"/usr/bin/nspr-config
	install -Dm644 nspr.pc "$pkgdir"/usr/lib/pkgconfig/nspr.pc
	rm -rf "$pkgdir"/usr/bin/prerr.properties \
		"$pkgdir"/usr/bin/compile-et.pl \
		"$pkgdir"/usr/share/aclocal/nspr.m4 \
		"$pkgdir"/usr/include/nspr/md
}

sha512sums="502815833116e25f79ddf71d1526484908aa92fbc55f8a892729cb404a4daafcc0470a89854cd080d2d20299fdb7d9662507c5362c7ae661cbacf308ac56ef7f  nspr-4.35.tar.gz
1f694fc151f6578080449e3aa999c520486bbe117b8237150966ec43092db4156e81412ac889045e0c0c3bf65d459af5bdc1cf19c9fa3dab120405a60732f15a  stacksize.patch"
