kernel-mc README
--------------------------------------------------------------------------
This patchset is Horst Burkhardt's "mc" patchset. 
It is also the upstream for Adelie Linux' "easy-kernel". 

This patchset is designed for compatibility with all distributions, and 
is tuned for performance and memory consumption on all architectures. 

It integrates some of the patches from Gentoo Linux' "gentoo-sources" to 
leverage the bug-finding power of the Gentoo community.

Unless otherwise explicitly stated, the resulting kernel should be 
considered to be suitable under all loads and in all environments. 

Patchset Sequence
--------------------------------------------------------------------------
0100	 	cumulative linux patch (from kernel.org)
0120-0199 	security patches 
0200-0249 	arch-specific fixes 
0250-0299 	performance patches 
0300-0399 	device driver patches 
0400-0499 	kernel subsystem patches 
0500-0549 	feature additions 
0550-0599 	experimental patches
0600-0999 	not yet allocated
     1000 	version update patch

Individual Patch Descriptions (0120-1000):
--------------------------------------------------------------------------

File: 	0120-XATTR_USER_PREFIX.patch
From: 	Anthony G. Basile <blueness@gentoo.org> 
Desc: 	Support for namespace user.pax.* on tmpfs.

File: 	0122-link-security-restrictions.patch
From: 	Ben Hutchings <ben@decadent.org.uk>
Desc: 	Enable link security restrictions by default.

File: 	0124-bluetooth-keysize-check.patch
From: 	Marcel Holtmann <marcel@holtmann.org>
Desc: 	Check key sizes when Bluetooth "Secure Simple Pairing" is enabled.

File: 	0126-sign-file-libressl.patch
From: 	https://bugs.gentoo.org/717166 
Desc: 	Allow recent versions of LibreSSL to provide full functionality for sign-file. 

File:	0200-x86-compile.patch
From:	Laurent Bercot <ska-adelie@skarnet.org>
Desc:	Fixes builds on x86 that terminate due to overenthusiastic -Werror 

File:	0202-fix-gcc13-build.patch
From:	Sam James <sam@gentoo.org>
Desc:	Fixes building with gcc-13 due to plugin ABI mismatch, does not affect older gcc builds

File: 	0210-fix-powerbook-6-5-audio.patch
From: 	Horst Burkhardt <horst@burkhardt.com.au> 
Desc: 	Enables audio in PowerBook6,4 and PowerBook6,5 iBooks on PowerPC

File: 	0255-ultraksm.patch
From: 	https://github.com/sirlucjan/kernel-patches/
Desc: 	Ultra Same-Page Merging provides an aggressive KSM implementation to further enhance memory usage over RedHat KSM in mainline

File: 	0260-reduce-swappiness.patch
From: 	Horst Burkhardt <horst@burkhardt.com.au> - originally from -ck patchset by Con Kolivas	
Desc: 	Reduces the proclivity of the kernel to page out memory contents to disk 

File: 	0300-tmp513-regression-fix.patch
From: 	Mike Pagano <mpagano@gentoo.org>
Desc: 	Fix to regression in Kconfig from kernel 5.5.6 to enable tmp513 hardware monitoring module to build.

File: 	0500-print-fw-info.patch
From: 	Georgy Yakovlev <gyakovlev@gentoo.org> 
Desc: 	Makes kernel print exact firmware file that kernel attempts to load. 

File: 	0502-gcc9-kcflags.patch
From: 	https://github.com/graysky2/kernel_compiler_patch/
Desc: 	Enables gcc >=9.1 optimizations for the very latest x86_64 CPUs.

File: 	1000-version.patch
From: 	Horst Burkhardt <horst@burkhardt.com.au> 
Desc: 	Adjust Makefile to represent patchset version, adds cool logo to boot logo options
