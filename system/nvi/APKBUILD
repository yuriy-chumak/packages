# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=nvi
pkgver=1.81.6
_debver=15
pkgrel=1
pkgdesc="Berkeley text editor"
url="https://sites.google.com/a/bostic.com/keithbostic/vi/"
arch="all"
options="!check"  # No test suite.
license="BSD-4-Clause-UC"
depends=""
makedepends="db-dev ncurses-dev"
subpackages="$pkgname-doc"
source="http://deb.debian.org/debian/pool/main/n/nvi/nvi_$pkgver.orig.tar.gz
	http://deb.debian.org/debian/pool/main/n/nvi/nvi_$pkgver-$_debver.debian.tar.xz
	build-fix.patch
	"

# secfixes:
#   1.81.6-r1:
#     - CVE-2001-1562

prepare() {
	default_prepare
	while read -r i; do
		msg "$i"
		patch -p1 -i "../debian/patches/$i"
	done < ../debian/patches/series

	cd "$builddir"/dist
	chmod u+w config.sub
	update_config_sub
}

build() {
	mkdir -p "$builddir"/build
	cd "$builddir"/build
	# Note!  --disable-curses means disable *builtin* curses.
	# It makes vi(1) use ncurses instead.  That is what we want.
	LIBS="-ltinfow" ../dist/configure \
		--prefix=/usr \
		--mandir="$pkgdir"/usr/share/man \
		--build=$CBUILD \
		--host=$CHOST \
		--disable-curses
	make
}

package() {
	cd "$builddir"/build
	mkdir -p "$pkgdir/usr/bin"
	make prefix="$pkgdir/usr" install -j1
	mv "$pkgdir"/usr/bin/ex "$pkgdir"/usr/bin/ex.nvi
	mv "$pkgdir"/usr/share/man/cat1/ex.0 "$pkgdir"/usr/share/man/cat1/ex.nvi.0
	mv "$pkgdir"/usr/share/man/man1/ex.1 "$pkgdir"/usr/share/man/man1/ex.nvi.1
	mv "$pkgdir"/usr/bin/view "$pkgdir"/usr/bin/view.nvi
	mv "$pkgdir"/usr/share/man/cat1/view.0 "$pkgdir"/usr/share/man/cat1/view.nvi.0
	mv "$pkgdir"/usr/share/man/man1/view.1 "$pkgdir"/usr/share/man/man1/view.nvi.1
}

sha512sums="1be798daf0cd05010ddaf0aa0510dc799708fd79d4b243e2700adff18e931ddd9d11621796fa8086088c3e93ba20f15ab86783732665169c52b73eaf587ff0b3  nvi_1.81.6.orig.tar.gz
7756ca0ba9503516fcbf1a5fe51465bebf74bf107e040cea80de0dd501854fe0f25d67ddbd4142e58057ad015be12f8b711ef861fd6ad3f317bc619327579513  nvi_1.81.6-15.debian.tar.xz
b84cfb83ea5ed15f5c7d9e608771a2248531be388fa19340e90074d37e10f6f75c8b965a40f5288275973f23ed6fda54aa4ef990b43d4828d174b66fd3ced1e9  build-fix.patch"
