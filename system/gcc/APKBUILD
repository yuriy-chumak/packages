# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=gcc
pkgver=13.3.0
[ "$BOOTSTRAP" = "nolibc" ] && pkgname="gcc-pass2"
[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

pkgname="$pkgname$_target"
pkgrel=0
pkgdesc="The GNU Compiler Collection"
url="https://gcc.gnu.org"
arch="all"
license="GPL-3.0+ AND LGPL-2.1+"
_gccrel=$pkgver-r$pkgrel
depends="binutils$_target isl"
makedepends_build="gcc$_cross g++$_cross byacc flex texinfo zip gmp-dev
	mpfr-dev mpc1-dev zlib-dev"
makedepends_host="linux-headers gmp-dev mpfr-dev mpc1-dev isl-dev zlib-dev
	libucontext-dev"
subpackages=" "
[ "$CHOST" = "$CTARGET" ] && subpackages="gcc-doc$_target gcc-lang$_target"
replaces="libstdc++ binutils"
options="$options !check !dbg strip toolchain"

: ${LANG_CXX:=true}
: ${LANG_OBJC:=true}
: ${LANG_GO:=true}
: ${LANG_FORTRAN:=true}
: ${LANG_ADA:=true}

LIBGOMP=true
LIBGCC=true
LIBATOMIC=true
LIBITM=true
LIBSANITIZER=true

if [ "$CHOST" != "$CTARGET" ]; then
	if [ "$BOOTSTRAP" = nolibc ]; then
		LANG_CXX=false
		LANG_ADA=false
		LIBGCC=false
		_builddir="$srcdir/build-cross-pass2"
	else
		_builddir="$srcdir/build-cross-final"
	fi
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LIBGOMP=false
	LIBATOMIC=false
	LIBITM=false
	LIBSANITIZER=false

	# reset target flags (should be set in crosscreate abuild)
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS $CFLAGS"
	export CXXFLAGS="$CPPFLAGS $CXXFLAGS"
	unset CPPFLAGS
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	STRIP_FOR_TARGET="$CTARGET-strip"
elif [ "$CBUILD" != "$CHOST" ]; then
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS $CFLAGS"
	export CXXFLAGS="$CPPFLAGS $CXXFLAGS"
	unset CPPFLAGS

	# reset flags and cc for build
	export CC_FOR_BUILD="gcc"
	export CXX_FOR_BUILD="g++"
	export CFLAGS_FOR_BUILD=" "
	export CXXFLAGS_FOR_BUILD=" "
	export LDFLAGS_FOR_BUILD=" "
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	# Languages that do not need bootstrapping
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false

	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build-cross-native"
else
	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build"
fi

# libitm has TEXTRELs in ARM build, so disable for now
# gcc itself has TEXTRELs in m68k; until this is fixed, we need it
case "$CTARGET_ARCH" in
arm*)		LIBITM=false ;;
m68k)		options="$options textrels" ;;
ppc)		options="$options textrels" ;;
esac

# Fortran uses libquadmath if toolchain has __float128
# currently on x86, x86_64 and ia64
LIBQUADMATH=$LANG_FORTRAN
case "$CTARGET_ARCH" in
pmmx | x86 | x86_64)	LIBQUADMATH=$LANG_FORTRAN ;;
*)			LIBQUADMATH=false ;;
esac

# libatomic is a dependency for openvswitch
$LIBATOMIC && subpackages="$subpackages libatomic::$CTARGET_ARCH"
$LIBGCC && subpackages="$subpackages libgcc::$CTARGET_ARCH"
$LIBQUADMATH && subpackages="$subpackages libquadmath::$CTARGET_ARCH"
$LIBSANITIZER && subpackages="$subpackages libsanitizer::$CTARGET_ARCH"
if $LIBGOMP; then
	depends="$depends libgomp=$_gccrel"
	subpackages="$subpackages libgomp::$CTARGET_ARCH"
fi

_languages=c
if $LANG_CXX; then
	subpackages="$subpackages libstdc++:libcxx:$CTARGET_ARCH g++$_target:gpp"
	_languages="$_languages,c++"
fi
if $LANG_OBJC; then
	subpackages="$subpackages libobjc::$CTARGET_ARCH gcc-objc$_target:objc"
	_languages="$_languages,objc"
fi
if $LANG_GO; then
	subpackages="$subpackages libgo::$CTARGET_ARCH gcc-go$_target:go"
	_languages="$_languages,go"
fi
if $LANG_FORTRAN; then
	subpackages="$subpackages libgfortran::$CTARGET_ARCH gfortran$_target:gfortran"
	_languages="$_languages,fortran"
fi
if $LANG_ADA; then
	subpackages="$subpackages libgnat::$CTARGET_ARCH gcc-gnat$_target:gnat"
	_languages="$_languages,ada"
	makedepends_build="$makedepends_build gcc-gnat gcc-gnat$_cross"
fi
makedepends="$makedepends_build $makedepends_host"

source="https://ftp.gnu.org/gnu/gcc/gcc-$pkgver/gcc-$pkgver.tar.xz

	0002-posix_memalign.patch
	0012-static-pie.patch

	002_all_default-relro.patch
	005_all_default-as-needed.patch
	007_all_alpha-mieee-default.patch
	011_all_default-warn-format-security.patch
	012_all_default-warn-trampolines.patch
	020_all_msgfmt-libstdc++-link.patch
	050_all_sanitizer-lfs.patch
	051_all_libiberty-pic.patch
	076_all_match.pd-don-t-emit-label-if-not-needed.patch
	078_all_match.pd-CSE-the-dump-output-check.patch
	092_all_riscv_PR109760-gstreamer.patch

	101-pr115836.patch

	201-ada.patch
	202-ibm-ldbl.patch
	205-nopie.patch
	206-arm-unwind-functionise.patch

	libgcc-always-build-gcceh.a.patch
	gcc-4.9-musl-fortify.patch
	gcc-6.1-musl-libssp.patch
	gcc-pure64.patch

	fix-cxxflags-passing.patch
	ada-shared.patch

	330-gccgo-link-to-ucontext.patch
	331-gccgo-use-real-off_t-type.patch
	332-gccgo-sysinfo.patch
	334-gccgo-signal-shell.patch
	335-gccgo-signal-ppc32.patch
	341-gccgo-libucontext-stack.patch
	342-gccgo-reflect-underscore.patch
	libgo-musl-1.2.3.patch

	add-classic_table-support.patch
	gcc-5.4.0-locale.patch

	sanitation.patch
	risc-san.patch

	match-split.patch
	insn-split.patch
	"

# we build out-of-tree
_gccdir="$srcdir"/gcc-$pkgver
_gcclibdir=/usr/lib/gcc/${CTARGET}/$pkgver
_gcclibexec=/usr/libexec/gcc/${CTARGET}/$pkgver

prepare() {
	cd "$_gccdir"

	_err=
	for i in $source; do
		case "$i" in
		*.patch)
			msg "Applying $i"
			patch -p1 -F3 -i "$srcdir"/$i || _err="$_err $i"
			;;
		esac
	done

	if [ -n "$_err" ]; then
		error "The following patches failed:"
		for i in $_err; do
			echo "  $i"
		done
		return 1
	fi

	echo ${pkgver} > gcc/BASE-VER
}

build() {
	local _arch_configure=
	local _libc_configure=
	local _cross_configure=
	local _bootstrap_configure=
	local _hash_style=gnu
	local _symvers=

	cd "$_gccdir"

	case "$CTARGET" in
	aarch64*-*-*-*)		_arch_configure="--with-arch=armv8-a --with-abi=lp64";;
	armv5-*-*-*eabi)	_arch_configure="--with-arch=armv5te --with-tune=arm926ej-s --with-float=soft --with-abi=aapcs-linux";;
	armv6-*-*-*eabihf)	_arch_configure="--with-arch=armv6zk --with-tune=arm1176jzf-s --with-fpu=vfp --with-float=hard --with-abi=aapcs-linux";;
	armv7-*-*-*eabihf)	_arch_configure="--with-arch=armv7-a --with-tune=generic-armv7-a --with-fpu=vfpv3-d16 --with-float=hard --with-abi=aapcs-linux --with-mode=thumb";;
	m68k-*-*-*)		_arch_configure="--with-arch=68020 --with-tune=68020-40";;
	mipsel-*-*-*)		_arch_configure="--with-arch-32=mips2 --with-tune-32=mips32 --with-fp-32=32 --with-mips-plt --with-float=hard --with-abi=32";;
	mips-*-*-*)		_arch_configure="--with-arch=mips3 --with-mips-plt --with-abi=32"; _hash_style="sysv";;
	mips32el-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-abi=32"; _hash_style="sysv";;
	powerpc-*-*-*)		_arch_configure="--enable-secureplt --enable-decimal-float=no";;
	powerpc64le*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no";;
	powerpc64*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no";;
	i486-*-*-*)		_arch_configure="--with-arch=i486 --with-tune=generic --enable-cld";;
	i586-*-*-*)		_arch_configure="--with-arch=i586 --with-tune=pentium2 --enable-cld --enable-mmx";;
	pentium3-*-*-*)		_arch_configure="--with-arch=pentium3 --with-tune=pentium-m";;
	s390x-*-*-*)		_arch_configure="--with-arch=z196 --with-tune=zEC12 --with-zarch --with-long-double-128 --enable-decimal-float";;
	esac

	case "$CTARGET_LIBC" in
	musl)
		_symvers="--disable-symvers"
		export libat_cv_have_ifunc=no
		export ac_cv_type_off64_t=no
		;;
	esac

	[ "$CBUILD" != "$CHOST"   ] && _cross_configure="--disable-bootstrap"
	[ "$CHOST"  != "$CTARGET" ] && _cross_configure="--disable-bootstrap --with-sysroot=$CBUILDROOT"

	case "$BOOTSTRAP" in
	nolibc)	_bootstrap_configure="--with-newlib --disable-shared --enable-threads=no" ;;
	*)	_bootstrap_configure="--enable-shared --enable-threads --enable-tls" ;;
	esac

	$LIBGOMP	|| _bootstrap_configure="$_bootstrap_configure --disable-libgomp"
	$LIBATOMIC	|| _bootstrap_configure="$_bootstrap_configure --disable-libatomic"
	$LIBITM		|| _bootstrap_configure="$_bootstrap_configure --disable-libitm"
	$LIBSANITIZER	|| _bootstrap_configure="$_bootstrap_configure --disable-libsanitizer"
	$LIBQUADMATH	|| _arch_configure="$_arch_configure --disable-libquadmath"

	msg "Building the following:"
	echo ""
	echo "  CBUILD=$CBUILD"
	echo "  CHOST=$CHOST"
	echo "  CTARGET=$CTARGET"
	echo "  CTARGET_ARCH=$CTARGET_ARCH"
	echo "  CTARGET_LIBC=$CTARGET_LIBC"
	echo "  languages=$_languages"
	echo "  arch_configure=$_arch_configure"
	echo "  libc_configure=$_libc_configure"
	echo "  cross_configure=$_cross_configure"
	echo "  bootstrap_configure=$_bootstrap_configure"
	echo "  hash_style=$_hash_style"
	echo ""

	export enable_libstdcxx_time=rt

	mkdir -p "$_builddir"
	cd "$_builddir"
	"$_gccdir"/configure --prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--build=${CBUILD} \
		--host=${CHOST} \
		--target=${CTARGET} \
		--with-pkgversion="Adelie ${pkgver}" \
		--with-bugurl="https://bts.adelielinux.org/" \
		--enable-checking=release \
		--disable-fixed-point \
		--disable-libstdcxx-pch \
		--disable-multiarch \
		--disable-multilib \
		--disable-werror \
		$_symvers \
		--enable-__cxa_atexit \
		--enable-default-pie \
		--enable-default-ssp \
		--enable-cloog-backend \
		--enable-languages=$_languages \
		--enable-linker-build-id \
		$_arch_configure \
		$_libc_configure \
		$_cross_configure \
		$_bootstrap_configure \
		--with-matchpd-partitions=32 \
		--with-system-zlib \
		--with-linker-hash-style=$_hash_style
	make \
		STAGE1_CFLAGS="$CFLAGS" STAGE1_LDFLAGS="$LDFLAGS" \
		BOOT_CFLAGS="$CFLAGS" BOOT_LDFLAGS="$LDFLAGS"
}

check() {
	cd "$_builddir"
	make check
}

package() {
	cd "$_builddir"
	make -j1 DESTDIR="${pkgdir}" install

	ln -s gcc "$pkgdir"/usr/bin/cc

	# strip debug info from some static libs
	${STRIP_FOR_TARGET} -g `find "$pkgdir" \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		-o -name libmudflap.a -o -name libmudflapth.a \
		-o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		-o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		-o -name libatomic.a -o -name libasan.a -o -name libtsan.a \) \
		-a -type f`

	if $LIBGOMP; then
		mv "$pkgdir"/usr/lib/libgomp.spec "$pkgdir"/$_gcclibdir
	fi
	if $LIBITM; then
		mv "$pkgdir"/usr/lib/libitm.spec "$pkgdir"/$_gcclibdir
	fi

	# remove ffi
	rm -f "$pkgdir"/usr/lib/libffi* "$pkgdir"/usr/share/man/man3/ffi*
	find "$pkgdir" -name 'ffi*.h' | xargs rm -f

	local gdblib="${_target:+$CTARGET/}lib"
	for i in $(find "$pkgdir"/usr/$gdblib/ -type f -maxdepth 1 -name "*-gdb.py" ); do
		mkdir -p "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib
		mv "$i" "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib/
	done

	# move ada runtime libs
	if $LANG_ADA; then
		for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.so"); do
			mv "$i" "$pkgdir"/usr/lib/
			ln -s ../../../../${i##*/} $i
		done
	fi

	if [ "$CHOST" != "$CTARGET" ]; then
		# cross-gcc: remove any files that would conflict with the
		# native gcc package
		rm -rf "$pkgdir"/usr/bin/cc "$pkgdir"/usr/include "$pkgdir"/usr/share
		# libcc1 does not depend on target, don't ship it
		rm -rf "$pkgdir"/usr/lib/libcc1.so*

		# fixup gcc library symlinks to be linker scripts so
		# linker finds the libs from relocated sysroot
		for so in "$pkgdir"/usr/$CTARGET/lib/*.so; do
			if [ -h "$so" ]; then
				local _real="$(basename $(readlink "$so"))"
				rm -f "$so"
				echo "GROUP ($_real)" > "$so"
			fi
		done
	fi
}

libatomic() {
	pkgdesc="GCC Atomic library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libatomic.so.* "$subpkgdir"/usr/lib/
}

libcxx() {
	pkgdesc="GNU C++ standard runtime library"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libstdc++.so.* "$subpkgdir"/usr/lib/
}

gpp() {
	pkgdesc="GNU C++ standard library and compiler"
	depends="libstdc++=$_gccrel gcc=$_gccrel libc-dev"
	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/${_target:+$CTARGET/}include \
		"$subpkgdir"/usr/${_target:+$CTARGET/}lib \

	mv "$pkgdir/$_gcclibexec/cc1plus" "$subpkgdir/$_gcclibexec/"

	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/*++* "$subpkgdir"/usr/${_target:+$CTARGET/}lib/
	mv "$pkgdir"/usr/${_target:+$CTARGET/}include/c++ "$subpkgdir"/usr/${_target:+$CTARGET/}include/
	mv "$pkgdir"/usr/bin/*++ "$subpkgdir"/usr/bin/
}

libobjc() {
	pkgdesc="GNU Objective-C runtime"
	replaces="objc"
	depends=
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libobjc.so.* "$subpkgdir"/usr/lib/
}

objc() {
	pkgdesc="GNU Objective-C"
	replaces="gcc"
	depends="libc-dev gcc=$_gccrel libobjc=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibdir/include \
		"$subpkgdir"/usr/lib
	mv "$pkgdir"/$_gcclibdir/include/objc "$subpkgdir"/$_gcclibdir/include/
	mv "$pkgdir"/usr/lib/libobjc.so "$pkgdir"/usr/lib/libobjc.a \
		"$subpkgdir"/usr/lib/
}

libgcc() {
	pkgdesc="GNU C compiler runtime libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgcc_s.so.* "$subpkgdir"/usr/lib/
}

libgomp() {
	pkgdesc="GCC shared-memory parallel programming API library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgomp.so.* "$subpkgdir"/usr/lib/
}

libgo() {
	pkgdesc="Go runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgo.so.* "$subpkgdir"/usr/lib/
}

go() {
	pkgdesc="Go support for GCC"
	# Symbols for libucontext are required for proper operation.
	# The unwinder may fail to work, causing stack smashing or hangs.
	# See #832
	depends="gcc=$_gccrel libgo=$_gccrel libucontext-dbg"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/go "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/*gccgo "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/*go "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gofmt "$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/go1 "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/cgo "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/buildid "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/test2json "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/vet "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/usr/lib/libgo.a \
		"$pkgdir"/usr/lib/libgo.so \
		"$pkgdir"/usr/lib/libgobegin.a \
		"$pkgdir"/usr/lib/libgolibbegin.a \
		"$subpkgdir"/usr/lib/
}

libgfortran() {
	pkgdesc="Fortran runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgfortran.so.* "$subpkgdir"/usr/lib/
}

libquadmath() {
	replaces="gcc"
	pkgdesc="128-bit math library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquadmath.so.* "$subpkgdir"/usr/lib/
}

libsanitizer() {
	pkgdesc="Runtime code linting libraries for GCC"
	depends=

	# https://github.com/google/sanitizers/issues/794
	# https://reviews.llvm.org/D28609 ("ported to GCC")
	case $CTARGET_ARCH in
	pmmx|x86|i528|armel|armhf|armv7|m68k|mips32|mips32el|ppc)
		sanitizer_extras="";;
	*)
		sanitizer_extras="lsan tsan";;
	esac

	mkdir -p "$subpkgdir"/usr/lib
	for san in asan sanitizer ubsan ${sanitizer_extras}; do
		mv "$pkgdir"/usr/lib/lib${san}* "$subpkgdir"/usr/lib/
	done

	mkdir -p "$subpkgdir"/$_gcclibdir/include
	mv "$pkgdir"/$_gcclibdir/include/sanitizer \
		"$subpkgdir"/$_gcclibdir/include/
}

gfortran() {
	pkgdesc="GNU Fortran Compiler"
	depends="gcc=$_gccrel libgfortran=$_gccrel"
	$LIBQUADMATH && depends="$depends libquadmath=$_gccrel"
	replaces="gcc"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gfortran "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgfortran.a \
		"$pkgdir"/usr/lib/libgfortran.so \
		"$subpkgdir"/usr/lib/
	if $LIBQUADMATH; then
		mv "$pkgdir"/usr/lib/libquadmath.a \
			"$pkgdir"/usr/lib/libquadmath.so \
			"$subpkgdir"/usr/lib/
	fi
	mv "$pkgdir"/$_gcclibexec/f951 "$subpkgdir"/$_gcclibexec
	mv "$pkgdir"/usr/lib/libgfortran.spec "$subpkgdir"/$_gcclibdir
}

libgnat() {
	pkgdesc="GNU Ada runtime shared libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.so "$subpkgdir"/usr/lib/
}

gnat() {
	pkgdesc="Ada support for GCC"
	depends="gcc=$_gccrel"
	[ "$CHOST" = "$CTARGET" ] && depends="$depends libgnat=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/*gnat* "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibdir/*ada* "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/usr/bin/*gnat* "$subpkgdir"/usr/bin/
}

sha512sums="ed5f2f4c6ed2c796fcf2c93707159e9dbd3ddb1ba063d549804dd68cdabbb6d550985ae1c8465ae9a336cfe29274a6eb0f42e21924360574ebd8e5d5c7c9a801  gcc-13.3.0.tar.xz
6d84354e6df96d5ea244eb3bb5f044781796b88040b11c78fb6ee509e5aac19d46e0e92ca836e98e6495d9751f52439833b748efc419e4f5d5301fb549c4dcc9  0002-posix_memalign.patch
f0bda72a54d2a81c4e7b27cc06347409d9e3be708d51e280ad1130c7e1d2db6e0c4e7b2dd3c606f6a4ec78c1eb293a26f76dda8210124d9f928f351d46be7d7e  0012-static-pie.patch
2cb8f3773897e34d2d194076396d7fc6413d28b5f0546d092f83c22f229936f7e329b3eaaf86a6d22e0f609ae2b66e82b656685e9e9a51ed2f89e9efaa5e9534  002_all_default-relro.patch
879c377122af10a4e444ab67cfe81927cc4e574c63d0a619c20f094e87218ab7961dcc96d93830891af688182a5feafeb787d79b58a5125cf20258baa13c94d0  005_all_default-as-needed.patch
b0a6117aad4f72d62777004fbb0a93cb0ae613d44846162556535f1751be998e6cde61329451026bc539d119ae2d54b42eaff54b92f2f68cb14c2b1384cbc928  007_all_alpha-mieee-default.patch
abf28cf8495ce63eeeb9704a83e889c9b7819cc0394807ba966fe57dadc5f1e04478edc6d28af293330286dc70af4b513376bf53ddf705810c3b721b5089c511  011_all_default-warn-format-security.patch
6fa1f7bb936f8c634579689aa5b36a23a89e1f065514b1c946a410cd0057744135db833bb5cbb86a15738ebac787b5803bba0a2d4d0da628dfbd23d9bcd577b4  012_all_default-warn-trampolines.patch
f20ba1e7a6febf099a79c4b4251e15331e6de13531b787c2e2d14c908e3ff1e0e34a79824e7f6470331adf9c01c3302813dfbd508fec021a7f86f833c806806b  020_all_msgfmt-libstdc++-link.patch
8cadf32d4f9e58ae174640be419478ee709702c1df3faffb2cfc4e48418260b8bbadf6c18fbaf43cc32aacb889f090045265c3f51e9366208837a46cbf68ab6e  050_all_sanitizer-lfs.patch
ec0a6613a21bd8352b73f693ee6055e81e4ef056b8b2b9916aa075649477b0c3f855fcfb37aa53301a559f05e5cfa41661c274f3a3c6cabb44a7adecf239473a  051_all_libiberty-pic.patch
0d505f08842bd75c686b18f50bc988078dce07b69ef8b6caa53e5e3a93e39527ab16f1d32a7f1f132279c3ac430a10b4e1aad7fc0bc2b6560fc33a1b65f6c109  076_all_match.pd-don-t-emit-label-if-not-needed.patch
b3cb3cad951c518b399f4fd0d4be07089ca1abf7a3271aaac4c25d8cd34a9b6d2bd6fa68164cf7f3967813f019db8dcf0ae8746f94ac6a4b85209c4012a8936f  078_all_match.pd-CSE-the-dump-output-check.patch
ce44d0bfa646394114c6e77f70da26ac4879cb2198a3bc4108ce080b7a247d6c0b0213c02b9bacbcd0b477a8e22a831a7dae3691f4f0462eaddecb457cd9eee6  092_all_riscv_PR109760-gstreamer.patch
eade22edbb70b57b06349bbf2f39def90bbe4e3eeb0d121dd2e07e1deaf73407e5247698d05660de67072672534461442b45e314d53daf691a10275f60fdc51c  101-pr115836.patch
ad971ebaa77907a26e4c268feb052d06b2df1530ed39b9c684a49bdf1024ad6b439bb8b28a957327635c83a97dcca3cee0875cfe9836f8fc3e06cdb137281e42  201-ada.patch
80748e1f6a287a0f645eecac54c5181156a20ab0facc826e5b1768fec87df9d4e9fd44362370dbbe5f78b25c3bba3a20a6acfbe6ce25ddd99bd6d085bfc6c112  202-ibm-ldbl.patch
ae30090390e3604e1e508edb36bc3a01283fe28657d63ae6d6238dd9f55cce587552c5a482425673aff0d0ccc9d99a649e24b552573e7b2ea29063fc5c0aa497  205-nopie.patch
ae2c2fe6abe678e34d60cb05e42c60d6b499f206d965066ee8bd26f9cabfbb69cab0f0061731d21b96a5f2ad67bebc4b49dbe39cbdd01dce8b61f829f4b3f708  206-arm-unwind-functionise.patch
3c66bbcde298cdc188bb68dfe16e074bda7355c2307b8263c75cb250e244dd604faf2e4bf782f8f39160892b5a433996615fd0597ce5bd20bd5c1b766bf12d2d  libgcc-always-build-gcceh.a.patch
40322bba8b3bfc26f0cd92bc9849c59560a6736dd22941bde4b6a1d9846e798d5acfe4fdc3200aafabbf6168349b08070a9e31d41967a48e84dbc608723cdcf3  gcc-4.9-musl-fortify.patch
7800fd0e1bcb33779c673c2effa8a61c4288979fba7d9dd215777ebec4d2cce5fe42fe72dcbce078bbc2268db58a25b42fc1a7327c1156e2db6a694b9303dbec  gcc-6.1-musl-libssp.patch
d8a184ff286516f49629fe97423b5584a7799069de2c3f0d45c2ff25be8e6e8706f9ca0e811f760cd3e0089bd82657091a2fa13139f4c3cc7d5d5dd25f82d5e8  gcc-pure64.patch
eb1d27d501e1c5099bbc476cda27738a13a3c27185d2e4f46c770dda30b7b0e774b58a62afad8ead44298296ce60b409fbab2da08bf114a66fa269f02d4132d7  fix-cxxflags-passing.patch
16d66b34de05a220d43ba0327bc75aabc107b36c629efa4b2f14662f6b1c805d8123aeb0a92e7e908c059b3d910acc2e7e1b50c7ac34a004d8a20b9a140427f4  ada-shared.patch
ce69e4cceb212e7b8e9af72a97b336dc3f38d7a30fc0c656a56ab3b431679a07efd746fd1cc39f913934f603c83085dd9e72a1b516636ba85f43b2cd96568404  330-gccgo-link-to-ucontext.patch
25223ce10dbeb6725834fbd2ea57752c9d7b96f9c1e0d1f46927c7b5236c1bed1361b28869e9830ad39818788e491609ef9a79e9255a565a2b6fb3c56c3bc48d  331-gccgo-use-real-off_t-type.patch
2b833b6bcba8eb8914499f3432679635eb1e186f0ab1b0cd2b0012edfe6c296f89c05097476817c9947a0f17ebe031cc1708335e6baa8d3d211116e10cbc7865  332-gccgo-sysinfo.patch
dc03c7b660f0142aa16e78e4e50581c883f6632f4794131f1131e0dc8fd500ba5d6a0046a36dc621c618a558b69e426a9bd1baea63faa5f64d7663915c062bbe  334-gccgo-signal-shell.patch
00f43d3d3f23c106ff020699397e82cf63ef30820e74ff3f7a82bcf55e5245011bb4a8ef84f0704287201ec8138692c548d4b6dae2d3e2aaf56e40d57ca2873d  335-gccgo-signal-ppc32.patch
3cbe5e879902a73121b22d903be605c7100e607864b0e305d6825a4083502fbe94be9a4166fb833b28e5d18cd7548f317719e28e3579194ea3e1b626450c943b  341-gccgo-libucontext-stack.patch
76d141a9e245595eab66cc4ddbfda57330790e04960de5d1c4e1e97efb52291d18087840f7494c0310afc3f093e1fad8c6070928f489c97201f32782f67559fd  342-gccgo-reflect-underscore.patch
fa59b0fb081d97f8f63506b8793699588a95c602b5d468140eb1e80456597e52e1cc45dc0b234ac8e60e2b0cd606d94d111c8b0ae64c0a2be1bc1b8a184ceb93  libgo-musl-1.2.3.patch
1860593584f629d24d5b6db14b0a3412e9f93449b663aaa4981301a0923db0159314905e694f27366fbfef72dce06636ab6df86862b7e9e9564847e03bee82c1  add-classic_table-support.patch
a09b3181002798d1b17b8374eba6bec18e67d4d4f30677311c330b599e231e97cf02c1b9b79c0829952f5027016e01146743b665b19558ed2693c60a567823fb  gcc-5.4.0-locale.patch
7fa3f66a9147e13091da7fdf15197aca85e0500756392e1c75eb2e2f77dce6d148277aea2a8cb463770f7b46424a94cb5f60514529a565b4ebb23236dcc7c366  sanitation.patch
f167da2df3c386dd2f4d39e1fef0964d0fbf7ba35b9432675ada61a7fe3213d4825b579ebb577f46b28c5556b2ada78ed6b7b8b6471b9d9a0684514a73650595  risc-san.patch
ff6159633f04d26eadc79895dc24ccb46671a04fdc728cbbac86964a14ce17e2e51cd7668947dfe06b9168bb9b8575a80955012e5f51295ea02f4f3169e07541  match-split.patch
ee626cbe4bdda5b868980c86ca066d33167d06517db676e43d0719c4ad7d11e99b3a0151927f15c93ab89f6c76dd12bd48d402d25771fa3fd175273248824eda  insn-split.patch"
