# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=skalibs
pkgver=2.14.3.0
pkgrel=0
pkgdesc="A set of general-purpose C programming libraries for skarnet.org software"
url="https://skarnet.org/software/skalibs/"
arch="all"
options="!check"  # No test suite.
license="ISC"
subpackages="$pkgname-dev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz skalibs.pc.in"

build() {
	./configure \
		--with-sysdeps-devurandom=yes \
		--with-sysdeps-posixspawnearlyreturn=no
	make
}

package() {
	make DESTDIR="$pkgdir" install
	sed -e "s/@@VERSION@@/$pkgver/g;" "$srcdir/$pkgname.pc.in" > "$srcdir/$pkgname.pc"
	install -D -m 0644 "$srcdir/$pkgname.pc" "$pkgdir/usr/lib/pkgconfig/$pkgname.pc"
	mkdir -p -m 0755 "$pkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$pkgdir/usr/share/doc/$pkgname"
}

dev() {
	default_dev
	mkdir -p -m 0755 "$subpkgdir/usr/lib/skalibs"
	mv "$pkgdir/usr/lib/skalibs/sysdeps" "$subpkgdir/usr/lib/skalibs/"
}

sha512sums="f40222740494425477252e78c772edda3f6cb201723558c513acb6c87d55a0f3432c918acf112c9457ed3ff0ee71bf5192f61b1c9070e668f219fd6d3f6f3bfa  skalibs-2.14.3.0.tar.gz
5771f0e72e7e7e9a45cafdf2e4508938a29ff9474845c897e383004b031ad142dee1b65d83a9a0168696782163cb4a32f82db4d7360413b433e68282cd4cc487  skalibs.pc.in"
