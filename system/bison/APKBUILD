# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bison
pkgver=3.8.2
pkgrel=0
pkgdesc="The GNU general-purpose parser generator"
arch="all"
# iconv issue:
# https://lists.gnu.org/archive/html/bug-bison/2020-07/msg00001.html
# https://www.openwall.com/lists/musl/2020/07/29/2
options="!check"  # see above
license="GPL-3.0+"
url="https://www.gnu.org/software/bison/bison.html"
depends="m4"
checkdepends="bison flex musl-locales"
makedepends="perl"
provider_priority=1
subpackages="$pkgname-doc $pkgname-lang"
source="https://ftp.gnu.org/gnu/bison/${pkgname}-${pkgver}.tar.xz"

# secfixes:
#   3.7-r0:
#     - CVE-2020-24240
#   3.5.4-r0:
#     - CVE-2020-14150

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--datadir=/usr/share \
		--infodir=/usr/share/info \
		--mandir=/usr/share/man
	make
}

check() {
	# Work around for Bison 3.4.x test failure
	for _calc in calc lexcalc mfcalc reccalc rpcalc; do
		make -j1 examples/c/$_calc/$_calc
	done
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="d4d23af6671406e97257892f90651b67f2ab95219831372be032190b7156c10a3435d457857e677445df8b2327aacccc15344acbbc3808a6f332a93cce23b444  bison-3.8.2.tar.xz"
