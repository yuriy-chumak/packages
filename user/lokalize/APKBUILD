# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lokalize
pkgver=22.04.2
pkgrel=0
pkgdesc="Computer-aided translation system"
url="https://kde.org/applications/development/org.kde.lokalize"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev qt5-qtscript-dev cmake extra-cmake-modules kauth-dev
	kcodecs-dev kcompletion-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev kiconthemes-dev kio-dev kitemviews-dev kjobwidgets-dev
	knotifications-dev kparts-dev kross-dev kservice-dev ktextwidgets-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev solid-dev sonnet-dev
	hunspell-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/lokalize-$pkgver.tar.xz
	tests.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f6a5da60e5d6155f2decab11a887fb3e13720857cafd54d435298c642c3bbc097e63389ebe5c031987ceb70f413f88abe66caa6213576abce9a48a73a4b00140  lokalize-22.04.2.tar.xz
9b71a96a8a72ee23fea7dd051028f6652842023bf027f82cfb65c12e39033e76f9f548cf6999dc9fca6ee459f6260888253e94ad6cb08ef9c12013a6faa663a8  tests.patch"
