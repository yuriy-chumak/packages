# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grantleetheme
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE Grantlee theme support library"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="grantlee-dev"
makedepends="$depends_dev qt5-qtbase-dev cmake extra-cmake-modules kauth-dev
	kcodecs-dev kconfigwidgets-dev kcoreaddons-dev kguiaddons-dev ki18n-dev
	kiconthemes-dev knewstuff-dev kservice-dev kwidgetsaddons-dev
	kxmlgui-dev attica-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantleetheme-$pkgver.tar.xz
	colour.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="431d8ab311f3257ca45786f09f058b5f69842058a4d17ea461935ffaeacb9f9f1ec0453117bae298536561e4eb346c70e9c41ce36269f8b6ee24c8e6e4cadec9  grantleetheme-22.04.2.tar.xz
4b204647d7e98812971ad9aabf9f719871ad02be50f6717d24d6ffc5ae982c307223acd0b4893c3a6534d52002870dc9a5f9b0fb1b9875b5699e2f85c98ae257  colour.patch"
