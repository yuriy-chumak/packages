# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Contributor: Dan Theisen <djt@hxx.in>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=mariadb
pkgver=10.6.4
pkgrel=1
pkgdesc="Open source database server"
url="https://mariadb.org/"
arch="all"
options="suid"	# required for pam_auth plugin, which is needed for
		# mysql_install_db to work
options="$options !dbg"  # abuild can't handle splitting -dbg with the way
                         # this build's package() function works.
license="GPL-2.0-only"
pkgusers="mysql"
pkggroups="mysql"
depends="$pkgname-common"
depends_dev="openssl-dev zlib-dev"
makedepends="$depends_dev bison cmake curl-dev libaio-dev libarchive-dev
	libedit-dev libevent-dev libexecinfo-dev libxml2-dev ncurses-dev
	linux-pam-dev pcre2-dev xz-dev"
_mytopdeps="perl perl-dbi perl-dbd-mysql perl-getopt-long perl-socket
	perl-term-readkey"
subpackages="$pkgname-client $pkgname-server $pkgname-libs $pkgname-embedded
	$pkgname-common $pkgname-backup mytop::noarch $pkgname-openrc
	$pkgname-doc $pkgname-dev"
source="https://archive.mariadb.org/$pkgname-$pkgver/source/$pkgname-$pkgver.tar.gz
	ppchax.patch
	missing-header.patch
	mariadb.initd
	mariadb-server.limits
	"

# secfixes:
#   10.6.4-r0:
#     - CVE-2020-13249
#     - CVE-2020-2814
#     - CVE-2020-2812
#     - CVE-2020-2760
#     - CVE-2020-2752
#     - CVE-2020-28912
#     - CVE-2020-14812
#     - CVE-2020-14789
#     - CVE-2020-14776
#     - CVE-2020-14765
#     - CVE-2021-27928
#     - CVE-2021-2166
#     - CVE-2021-2154
#     - CVE-2021-2389
#     - CVE-2021-2389
#   10.4.12-r0:
#     - CVE-2020-7221

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	_release="\
		-DBUILD_CONFIG=mysql_release \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_CXX_FLAGS=\"$CXXFLAGS -DNDEBUG\" \
		-DCMAKE_C_FLAGS=\"$CFLAGS -DNDEBUG\" \
		-DWITH_DEFAULT_COMPILER_OPTIONS=NO \
		-DWITH_DEFAULT_FEATURE_SET=NO "

	# NUMA is disabled because all arches do not support it
	# SKIP_TESTS skips client unittests that require a server to be present
	# JEMALLOC is reported to not work on musl, TODO: test this
	# ASAN also doesn't work on musl iirc
	_deps="\
		-DWITH_MARIABACKUP=ON \
		-DWITH_NUMA=OFF \
		-DWITH_LIBNUMA=OFF \
		-DWITH_LIBWSREP=ON \
		-DWITH_UNIT_TESTS=ON \
		-DWITH_LIBEDIT=ON \
		-DWITH_EMBEDDED_SERVER=ON \
		-DWITH_UNITTEST=OFF \
		-DWITH_ASAN=OFF \
		-DWITH_JEMALLOC=OFF \
		-DWITH_LIBWRAP=OFF \
		-DWITH_SYSTEMD=no \
		-DWITH_VALGRIND=OFF \
		-DWITH_COMMENT=\"${DISTRO_NAME:-Adélie Linux}\" \
		-DENABLED_LOCAL_INFILE=ON \
		-DENABLED_PROFILING=OFF \
		-DSKIP_TESTS=ON "

	# AWS_KEY_MANAGEMENT makes this package non-redistributable, disable it
	_plugins="\
		-DPLUGIN_ARCHIVE=YES \
		-DPLUGIN_ARIA=YES \
		-DPLUGIN_BLACKHOLE=YES \
		-DPLUGIN_CSV=YES \
		-DPLUGIN_MYISAM=YES \
		-DAUTH_GSSAPI_PLUGIN_TYPE=NO \
		-DPLUGIN_AUTH_GSSAPI=NO \
		-DPLUGIN_AWS_KEY_MANAGEMENT=NO \
		-DPLUGIN_CASSANDRA=NO \
		-DPLUGIN_FEEDBACK=NO "

	# Investigate turning more of these on
	_innodb="\
		-DWITH_INNODB_LZMA=YES \
		-DWITH_INNODB_BZIP2=NO \
		-DWITH_INNODB_LZ4=NO \
		-DWITH_INNODB_LZO=NO \
		-DWITH_INNODB_SNAPPY=NO "

	# MongoDB is not libre software, disable it
	_connect="\
		-DCONNECT_WITH_MYSQL=YES \
		-DCONNECT_WITH_MONGO=NO \
		-DCONNECT_WITH_ODBC=NO \
		-DCONNECT_WITH_JDBC=NO \
		-DCONNECT_WITH_LIBXML2=system "

	# non-portable things, things that don't work on musl, BE, 32-bit etc
	_evil="\
		-DPLUGIN_MROONGA=NO \
		-DPLUGIN_ROCKSDB=NO \
		-DPLUGIN_TOKUDB=NO "

	_system_libs="\
		-DWITH_LIBARCHIVE=system \
		-DWITH_PCRE=system \
		-DWITH_SSL=system \
		-DWITH_ZLIB=system \
		-DWITH_EXTERNAL_ZLIB=YES \
		-DCONC_WITH_EXTERNAL_ZLIB=YES "

	_system_paths="\
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSYSCONFDIR=/etc/mysql \
		-DMYSQL_DATADIR=/var/lib/mariadb \
		-DMYSQL_UNIX_ADDR=/var/run/mariadb/mysqld.sock \
		-DINSTALL_UNIX_ADDRDIR=/var/run/mariadb/mysqld.sock \
		-DPKG_CONFIG_EXECUTABLE=/usr/bin/pkgconf \
		-DINSTALL_BINDIR=bin \
		-DINSTALL_SCRIPTDIR=bin \
		-DINSTALL_SBINDIR=sbin \
		-DINSTALL_INCLUDEDIR=include/mysql \
		-DINSTALL_LIBDIR=lib \
		-DINSTALL_PLUGINDIR=lib/mariadb/plugin \
		-DINSTALL_DOCDIR=share/doc/$pkgname \
		-DINSTALL_DOCREADMEDIR=share/doc/$pkgname \
		-DINSTALL_MANDIR=share/man \
		-DINSTALL_MYSQLSHAREDIR=share/mariadb \
		-DINSTALL_SUPPORTFILESDIR=share/mariadb \
		-DSUFFIX_INSTALL_DIR=\"\" "

	_buildflags=$(echo "\
		${_release}\
		${_deps}\
		${_plugins}\
		${_innodb}\
		${_connect}\
		${_evil}\
		${_system_libs}\
		${_system_paths}\
		${CMAKE_CROSSOPTS}" | tr -d '\t')
	echo "Build Flags are: $_buildflags"
	eval "cmake $_buildflags ."
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '^(test-connect)$'
}

package() {
	depends="$pkgname-client $pkgname-server"
	install -m755 -D ${srcdir}/mariadb.initd ${pkgdir}/etc/init.d/mariadb
}

openrc() {
	default_openrc
	depends="mariadb-server=$pkgver-r$pkgrel"
}

_install_component() {
	cd "$builddir"
	component=$1
	prefix=$subpkgdir/usr
	msg "Installing component \"$component\" into $prefix"
	cmake -DCMAKE_INSTALL_PREFIX=${prefix} \
		-DCOMPONENT=${component} \
		-P cmake_install.cmake
}

libs() {
	pkgdesc="Libraries for the MariaDB database"
	_install_component SharedLibraries
}

client() {
	pkgdesc="The MariaDB database client"
	depends="mariadb-common=$pkgver-r$pkgrel"
	for part in Client ClientPlugins; do
		_install_component ${part}
	done
}

server() {
	pkgdesc="The MariaDB database server"
	depends="mariadb-common=$pkgver-r$pkgrel"
	install="$pkgname-server.pre-install"
	for part in Server Server_Scripts IniFiles connect-engine; do
		_install_component ${part}
	done

	install -m755 -D ${srcdir}/mariadb-server.limits \
		${subpkgdir}/etc/security/limits.d/mariadb.conf
}


common() {
	pkgdesc="MariaDB common files for both server and client"
	replaces="mysql-common"
	depends=

	SHARE_PATH=${pkgdir}-server/usr/share
	mkdir -p ${subpkgdir}/usr
	mv $SHARE_PATH ${subpkgdir}/usr

	_install_component Common
}

embedded() {
	pkgdesc="MariaDB embedded server and embeddable library"

	# move mysql-embedded from client subpackage to embedded
	mkdir -p ${subpkgdir}/usr/bin
	BIN_PATH=${pkgdir}-client/usr/bin
	mv $BIN_PATH/mysql_embedded ${subpkgdir}/usr/bin/

	# move embedded mysqld libraries from server package to embedded
	mkdir -p ${subpkgdir}/usr/lib
	LIB_PATH=${pkgdir}-server/usr/lib
	MYSQLD_ELIB=$(readlink $LIB_PATH/libmysqld.so)
	mv $LIB_PATH/$MYSQLD_ELIB ${subpkgdir}/usr/lib/
	mv $LIB_PATH/libmysqld.so ${subpkgdir}/usr/lib/
	mv $LIB_PATH/libmariadbd.so ${subpkgdir}/usr/lib/
}

backup() {
	pkgdesc="The mariabackup tool for physical online backups"
	_install_component backup
}

mytop() {
	pkgdesc="Command line tool used for monitoring MariaDB performance"
	depends="$_mytopdeps"
	_install_component Mytop
}

dev() {
	replaces="libmysqlclient mysql-dev"
	provides="mysql-dev=$pkgver-r$pkgrel"
	depends="$pkgname-libs"
	_install_component Development
}

doc() {
	for part in ManPagesClient ManPagesServer ManPagesDevelopment; do
		_install_component ${part}
	done
	default_doc
}

sha512sums="c216244ce126e6f8954303a76078dc12146e8ea941041edd215dc05201adda1d5158cd7f1c9831328f783aba4f884965ae288f33bbc43229bc6224a4f1079071  mariadb-10.6.4.tar.gz
0cb265173923cb85517a1144cfa904dafe059c1f859e13b4415442cabbb414db5904f2dcee0e865914b0f9c181d1d61d220834cb187b14f8538a0d2552025a9d  ppchax.patch
6618ee5608b06941da08872ea7ea16149b72a0174b157f970c03a9a9110efe0704c72742e63181f8059244e89fe25d1b07f1c87fed72115d62f347f58fafccdd  missing-header.patch
1a2b058aad5fd6d6964db58b663591299a5bf82e7ad21a105d53a3e7ad1c3674c737caf3ee12799cce947cf8ead1f3e789dd8afcfc769891f08208b3006f9654  mariadb.initd
d47324b34a87410eddb554f65199d145dfba9ae40407a62fb77df4e1ebf13087d15eb28e61e5e7cc9139eb42f093a0955136edae4020822652a1f0b4e219849e  mariadb-server.limits"
