# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=black-hole-solver
pkgver=1.12.0
pkgrel=0
pkgdesc="Move solver for various solitaire card games"
url="https://www.shlomifish.org/open-source/projects/black-hole-solitaire-solver/"
arch="all"
license="Expat"
depends=""
checkdepends="perl-dir-manifest perl-env-path perl-file-which perl-inline
	perl-inline-c perl-test-differences perl-test-trailingspace
	perl-test-some perl-test-trap"
makedepends="cmake perl-path-tiny rinutils python3"
subpackages="$pkgname-dev $pkgname-doc"
source="https://fc-solve.shlomifish.org/downloads/fc-solve/$pkgname-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -Bbuild \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make -C build
}

check() {
	rm t/perltidy.t  # test could fail on every bump
	rm t/valgrind.t  # we don't run Valgrind tests
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="54aea7d86717e6dd24eb809afe972691e644497eeb240859194eca6193506af9f5d739949ebc9f0776b7409b5f934a2e9ab3f4507dc827f90fd0c3de945818d1  black-hole-solver-1.12.0.tar.xz"
