# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=drkonqi
pkgver=5.24.5
pkgrel=0
pkgdesc="Crash diagnostic system for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11
license="GPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND BSD-2-Clause"
depends=""
makedepends="cmake extra-cmake-modules kauth-dev kcodecs-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev kidletime-dev
	kio-dev kitemviews-dev kjobwidgets-dev knotifications-dev kservice-dev
	kwallet-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev
	qt5-qtbase-dev qt5-qtx11extras-dev solid-dev syntax-highlighting-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/drkonqi-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e1b5986f687da629d5e5f3e9034f551db4fec26870ff6801922da64cb7cff6d3089ae171ede47a6a733bb383cbb6dd54a691db962829027ad1449e71f2c45405  drkonqi-5.24.5.tar.xz"
