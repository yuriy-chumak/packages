# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krdc
pkgver=22.04.2
pkgrel=0
pkgdesc="View and control remote desktops (VNC)"
url="https://www.kde.org/applications/internet/krdc/"
arch="all"
license="GPL-2.0-only"
depends=""
depends_dev="qt5-qtbase-dev kwallet-dev kconfig-dev kcoreaddons-dev"
makedepends="cmake extra-cmake-modules kcmutils-dev kdnssd-dev kiconthemes-dev
	knotifications-dev kbookmarks-dev kdoctools-dev kxmlgui-dev ki18n-dev
	kcompletion-dev kwallet-dev kwidgetsaddons-dev knotifyconfig-dev
	libvncserver-dev $depends_dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DWITH_RDP:BOOL=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="56e87131310708d81de044240917f6afa6d332209ef0468f73b944f3b5781e27b9adbb1b9bdde735d8a9aa5adf73f0c7142f37716b84e59307b39a31872db3fb  krdc-22.04.2.tar.xz"
