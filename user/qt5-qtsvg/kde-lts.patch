From df7c94a391e69adef6e2b7f3d415496c6ba62ab1 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Robert=20L=C3=B6hning?= <robert.loehning@qt.io>
Date: Wed, 17 Feb 2021 19:20:42 +0100
Subject: [PATCH 01/10] Avoid buffer overflow in isSupportedSvgFeature

Fixes oss-fuzz issue 29873.

Pick-to: 6.0 6.1
Change-Id: I382683aa2d7d3cf2d05a0b8c41ebf21d032fbd7c
Reviewed-by: Eirik Aavitsland <eirik.aavitsland@qt.io>
(cherry picked from commit afde7ca3a40f524e40052df696f74190452b22cb)
---
 src/svg/qsvgstructure.cpp | 8 ++++++--
 1 file changed, 6 insertions(+), 2 deletions(-)

diff --git a/src/svg/qsvgstructure.cpp b/src/svg/qsvgstructure.cpp
index b89608b..89c9e4e 100644
--- a/src/svg/qsvgstructure.cpp
+++ b/src/svg/qsvgstructure.cpp
@@ -255,9 +255,13 @@ inline static bool isSupportedSvgFeature(const QString &str)
     };
 
     if (str.length() <= MAX_WORD_LENGTH && str.length() >= MIN_WORD_LENGTH) {
+        const char16_t unicode44 = str.at(44).unicode();
+        const char16_t unicode45 = str.at(45).unicode();
+        if (unicode44 >= sizeof(asso_values) || unicode45 >= sizeof(asso_values))
+            return false;
         const int key = str.length()
-                        + asso_values[str.at(45).unicode()]
-                        + asso_values[str.at(44).unicode()];
+                        + asso_values[unicode45]
+                        + asso_values[unicode44];
         if (key <= MAX_HASH_VALUE && key >= 0)
             return str == QLatin1String(wordlist[key]);
     }
-- 
2.36.0

From b8a829e3883b8a4ac564ac25d20d099797644c24 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Robert=20L=C3=B6hning?= <robert.loehning@qt.io>
Date: Fri, 23 Jul 2021 13:53:47 +0200
Subject: [PATCH 02/10] Limit font size to avoid numerous overflows

The font size will be passed through a QFixed in
QFontEngineBox::ascent() and overflow there as well as in further places.

[ChangeLog] Avoid numerous overflows by limiting font size to 0xffff.
This fixes oss-fuzz issue 31701.

Pick-to: 5.15 6.1 6.2
Change-Id: I2d00c5639804af9b056f0efc979e9899c5358cf7
Reviewed-by: Eirik Aavitsland <eirik.aavitsland@qt.io>
(cherry picked from commit 76eeb072cdffc1a8c776ed01864e6751ccbfba85)
---
 src/svg/qsvghandler.cpp | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index 9dac05c..e496a54 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -1384,7 +1384,8 @@ static void parseFont(QSvgNode *node,
             break;
         case FontSizeValue: {
             QSvgHandler::LengthType dummy; // should always be pixel size
-            fontStyle->setSize(parseLength(attributes.fontSize, dummy, handler));
+            fontStyle->setSize(qMin(parseLength(attributes.fontSize, dummy, handler),
+                                    qreal(0xffff)));
         }
             break;
         default:
-- 
2.36.0

From 5b684556c7cbe136ff9f665b18a1e8c8e53f3175 Mon Sep 17 00:00:00 2001
From: Albert Astals Cid <albert.astals.cid@kdab.com>
Date: Mon, 11 Oct 2021 11:13:57 +0200
Subject: [PATCH 03/10] Support font size not in pixels

Fixes: QTBUG-97422
Pick-to: 6.2
Change-Id: I4df2af0e657f241af69480e6e30d454870df51d8
Reviewed-by: Eirik Aavitsland <eirik.aavitsland@qt.io>
(cherry picked from commit 4531aad935d55924a32212b339c657ce363a6c08)
---
 src/svg/qsvghandler.cpp | 7 ++++---
 1 file changed, 4 insertions(+), 3 deletions(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index e496a54..6862494 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -1383,9 +1383,10 @@ static void parseFont(QSvgNode *node,
         case FontSizeNone:
             break;
         case FontSizeValue: {
-            QSvgHandler::LengthType dummy; // should always be pixel size
-            fontStyle->setSize(qMin(parseLength(attributes.fontSize, dummy, handler),
-                                    qreal(0xffff)));
+            QSvgHandler::LengthType type;
+            qreal fs = parseLength(attributes.fontSize, type, handler);
+            fs = convertToPixels(fs, true, type);
+            fontStyle->setSize(qMin(fs, qreal(0xffff)));
         }
             break;
         default:
-- 
2.36.0

From dd33c643251fbdcc9ed91f3617646a4367a434b7 Mon Sep 17 00:00:00 2001
From: Albert Astals Cid <albert.astals.cid@kdab.com>
Date: Mon, 11 Oct 2021 11:11:26 +0200
Subject: [PATCH 04/10] Fix text x/y when the length is not in pixels

Fixes: QTBUG-97421
Pick-to: 6.2
Change-Id: I41f3cbf8e747530a67fe5074a988ba49aeb43b8e
Reviewed-by: Eirik Aavitsland <eirik.aavitsland@qt.io>
(cherry picked from commit fc1e1878743bcaac0c81a4748a00d0042cc15815)
---
 src/svg/qsvghandler.cpp | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index 6862494..ba894bc 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -3346,7 +3346,9 @@ static QSvgNode *createTextNode(QSvgNode *parent,
     //### editable and rotate not handled
     QSvgHandler::LengthType type;
     qreal nx = parseLength(x, type, handler);
+    nx = convertToPixels(nx, true, type);
     qreal ny = parseLength(y, type, handler);
+    ny = convertToPixels(ny, true, type);
 
     QSvgNode *text = new QSvgText(parent, QPointF(nx, ny));
     return text;
-- 
2.36.0

From 1f59a48d239045bda7cfd43ed48fbf8553d36756 Mon Sep 17 00:00:00 2001
From: Eirik Aavitsland <eirik.aavitsland@qt.io>
Date: Wed, 7 Jul 2021 10:09:58 +0200
Subject: [PATCH 05/10] Fix parsing of arc elements in paths

The arc element takes some flag parameters, which could be mixed up
with the float parameters since svg does not require delimiting
characters here. Hence legal svg would be misread..

Fixes: QTBUG-92184
Pick-to: 6.2 6.1 5.15
Change-Id: I5885c50d47e2e06ab0f02afefb7a5585c5c713ff
Reviewed-by: Paul Olav Tvete <paul.tvete@qt.io>
(cherry picked from commit b313862fa04d9a5403c16670a0d911eb3c633ee5)
---
 src/svg/qsvghandler.cpp                      | 19 ++++++++++++++++---
 tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp | 14 ++++++++++++++
 2 files changed, 30 insertions(+), 3 deletions(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index ba894bc..bfacd56 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -728,15 +728,25 @@ static QVector<qreal> parseNumbersList(const QChar *&str)
     return points;
 }
 
-static inline void parseNumbersArray(const QChar *&str, QVarLengthArray<qreal, 8> &points)
+static inline void parseNumbersArray(const QChar *&str, QVarLengthArray<qreal, 8> &points,
+                                     const char *pattern = nullptr)
 {
+    const size_t patternLen = qstrlen(pattern);
     while (str->isSpace())
         ++str;
     while (isDigit(str->unicode()) ||
            *str == QLatin1Char('-') || *str == QLatin1Char('+') ||
            *str == QLatin1Char('.')) {
 
-        points.append(toDouble(str));
+        if (patternLen && pattern[points.size() % patternLen] == 'f') {
+            // flag expected, may only be 0 or 1
+            if (*str != QLatin1Char('0') && *str != QLatin1Char('1'))
+                return;
+            points.append(*str == QLatin1Char('0') ? 0.0 : 1.0);
+            ++str;
+        } else {
+            points.append(toDouble(str));
+        }
 
         while (str->isSpace())
             ++str;
@@ -1631,8 +1641,11 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
         ++str;
         QChar endc = *end;
         *const_cast<QChar *>(end) = 0; // parseNumbersArray requires 0-termination that QStringRef cannot guarantee
+        const char *pattern = nullptr;
+        if (pathElem == QLatin1Char('a') || pathElem == QLatin1Char('A'))
+            pattern = "rrrffrr";
         QVarLengthArray<qreal, 8> arg;
-        parseNumbersArray(str, arg);
+        parseNumbersArray(str, arg, pattern);
         *const_cast<QChar *>(end) = endc;
         if (pathElem == QLatin1Char('z') || pathElem == QLatin1Char('Z'))
             arg.append(0);//dummy
diff --git a/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp b/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp
index 8f1f03b..36c76ec 100644
--- a/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp
+++ b/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp
@@ -74,6 +74,7 @@ private slots:
     void fillRule();
     void opacity();
     void paths();
+    void paths2();
     void displayMode();
     void strokeInherit();
     void testFillInheritance();
@@ -1047,6 +1048,19 @@ void tst_QSvgRenderer::paths()
     }
 }
 
+void tst_QSvgRenderer::paths2()
+{
+    const char *svg =
+        "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\">"
+            "<path d=\"M 3 8 A 5 5 0 1013 8\" id=\"path1\"/>"
+        "</svg>";
+
+    QByteArray data(svg);
+    QSvgRenderer renderer(data);
+    QVERIFY(renderer.isValid());
+    QCOMPARE(renderer.boundsOnElement(QLatin1String("path1")).toRect(), QRect(3, 8, 10, 5));
+}
+
 void tst_QSvgRenderer::displayMode()
 {
     static const char *svgs[] = {
-- 
2.36.0

From 8145bccbefe00a5daffd60c4e9fc11f551018df1 Mon Sep 17 00:00:00 2001
From: Allan Sandfeld Jensen <allan.jensen@qt.io>
Date: Fri, 5 Mar 2021 12:52:36 +0100
Subject: [PATCH 06/10] Improve parsing of "r"
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Negative r values are illegal, and zero means empty for circles.

Pick-to: 6.1
Change-Id: Icb1d932f35909f71dafe1ee69eb2250eeb1bb2ad
Reviewed-by: Mårten Nordheim <marten.nordheim@qt.io>
(cherry picked from commit 4a88e194e6b243e83703ad83d95e49b2febed99e)
---
 src/svg/qsvghandler.cpp | 9 ++++++---
 1 file changed, 6 insertions(+), 3 deletions(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index bfacd56..9575f14 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -2585,6 +2585,8 @@ static QSvgNode *createCircleNode(QSvgNode *parent,
     qreal ncx = toDouble(cx);
     qreal ncy = toDouble(cy);
     qreal nr  = toDouble(r);
+    if (nr < 0.0)
+        return nullptr;
 
     QRectF rect(ncx-nr, ncy-nr, nr*2, nr*2);
     QSvgNode *circle = new QSvgCircle(parent, rect);
@@ -3055,15 +3057,16 @@ static QSvgStyleProperty *createRadialGradientNode(QSvgNode *node,
 
     qreal ncx = 0.5;
     qreal ncy = 0.5;
-    qreal nr  = 0.5;
     if (!cx.isEmpty())
         ncx = toDouble(cx);
     if (!cy.isEmpty())
         ncy = toDouble(cy);
+
+    qreal nr = 0.0;
     if (!r.isEmpty())
         nr = toDouble(r);
-    if (nr < 0.5)
-        nr = 0.5;
+    if (nr <= 0.0)
+        return nullptr;
 
     qreal nfx = ncx;
     if (!fx.isEmpty())
-- 
2.36.0

From 4469006285be9994f7b8ab4587f089716f59ebb9 Mon Sep 17 00:00:00 2001
From: Eirik Aavitsland <eirik.aavitsland@qt.io>
Date: Fri, 2 Jul 2021 16:09:30 +0200
Subject: [PATCH 07/10] Fix parsing of animation clock values

Color animation duration parsing mixed seconds and milliseconds.

Factor out a common function for all clock value parsing, and
add checking for overflow and illegal values as a driveby..

Fixes: QTBUG-94878
Pick-to: 6.2 6.1 5.15
Change-Id: Ie1d974cd2db55a3d65d7ce02c373021021070489
Reviewed-by: Paul Olav Tvete <paul.tvete@qt.io>
(cherry picked from commit 1da0a668e52994832d8a048772bed65b61cb0e9b)
---
 src/svg/qsvghandler.cpp | 70 ++++++++++++++++++++---------------------
 1 file changed, 35 insertions(+), 35 deletions(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index 9575f14..b542089 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -2373,6 +2373,27 @@ static bool parseAnimateNode(QSvgNode *parent,
     return true;
 }
 
+static int parseClockValue(QString str, bool *ok)
+{
+    int res = 0;
+    int ms = 1000;
+    str = str.trimmed();
+    if (str.endsWith(QLatin1String("ms"))) {
+        str.chop(2);
+        ms = 1;
+    } else if (str.endsWith(QLatin1String("s"))) {
+        str.chop(1);
+    }
+    double val = ms * toDouble(str, ok);
+    if (ok) {
+        if (val > std::numeric_limits<int>::min() && val < std::numeric_limits<int>::max())
+            res = static_cast<int>(val);
+        else
+            *ok = false;
+    }
+    return res;
+}
+
 static bool parseAnimateColorNode(QSvgNode *parent,
                                   const QXmlStreamAttributes &attributes,
                                   QSvgHandler *handler)
@@ -2406,23 +2427,13 @@ static bool parseAnimateColorNode(QSvgNode *parent,
         }
     }
 
-    int ms = 1000;
-    beginStr = beginStr.trimmed();
-    if (beginStr.endsWith(QLatin1String("ms"))) {
-        beginStr.chop(2);
-        ms = 1;
-    } else if (beginStr.endsWith(QLatin1String("s"))) {
-        beginStr.chop(1);
-    }
-    durStr = durStr.trimmed();
-    if (durStr.endsWith(QLatin1String("ms"))) {
-        durStr.chop(2);
-        ms = 1;
-    } else if (durStr.endsWith(QLatin1String("s"))) {
-        durStr.chop(1);
-    }
-    int begin = static_cast<int>(toDouble(beginStr) * ms);
-    int end   = static_cast<int>((toDouble(durStr) + begin) * ms);
+    bool ok = true;
+    int begin = parseClockValue(beginStr, &ok);
+    if (!ok)
+        return false;
+    int end = begin + parseClockValue(durStr, &ok);
+    if (!ok || end <= begin)
+        return false;
 
     QSvgAnimateColor *anim = new QSvgAnimateColor(begin, end, 0);
     anim->setArgs((targetStr == QLatin1String("fill")), colors);
@@ -2512,24 +2523,13 @@ static bool parseAnimateTransformNode(QSvgNode *parent,
         }
     }
 
-    int ms = 1000;
-    beginStr = beginStr.trimmed();
-    if (beginStr.endsWith(QLatin1String("ms"))) {
-        beginStr.chop(2);
-        ms = 1;
-    } else if (beginStr.endsWith(QLatin1String("s"))) {
-        beginStr.chop(1);
-    }
-    int begin = static_cast<int>(toDouble(beginStr) * ms);
-    durStr = durStr.trimmed();
-    if (durStr.endsWith(QLatin1String("ms"))) {
-        durStr.chop(2);
-        ms = 1;
-    } else if (durStr.endsWith(QLatin1String("s"))) {
-        durStr.chop(1);
-        ms = 1000;
-    }
-    int end = static_cast<int>(toDouble(durStr)*ms) + begin;
+    bool ok = true;
+    int begin = parseClockValue(beginStr, &ok);
+    if (!ok)
+        return false;
+    int end = begin + parseClockValue(durStr, &ok);
+    if (!ok || end <= begin)
+        return false;
 
     QSvgAnimateTransform::TransformType type = QSvgAnimateTransform::Empty;
     if (typeStr == QLatin1String("translate")) {
-- 
2.36.0

From 53ada351e2e8ac14d2e428813e959689ca6fe583 Mon Sep 17 00:00:00 2001
From: Eirik Aavitsland <eirik.aavitsland@qt.io>
Date: Mon, 25 Oct 2021 14:17:55 +0200
Subject: [PATCH 08/10] Do stricter error checking when parsing path nodes
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

The SVG spec mandates that path parsing should terminate on the first
error encountered, and an error be reported. To improve the handling
of corrupt files, implement such error handling, and also limit the
number of QPainterPath elements to a reasonable range.

Fixes: QTBUG-96044
Pick-to: 6.2 5.15 5.12
Change-Id: Ic5e65d6b658516d6f1317c72de365c8c7ad81891
Reviewed-by: Allan Sandfeld Jensen <allan.jensen@qt.io>
Reviewed-by: Robert Löhning <robert.loehning@qt.io>
(cherry picked from commit 36cfd9efb9b22b891adee9c48d30202289cfa620)
---
 src/svg/qsvghandler.cpp | 59 +++++++++++++++++------------------------
 1 file changed, 25 insertions(+), 34 deletions(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index b542089..2ea80ed 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -1627,6 +1627,7 @@ static void pathArc(QPainterPath &path,
 
 static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
 {
+    const int maxElementCount = 0x7fff; // Assume file corruption if more path elements than this
     qreal x0 = 0, y0 = 0;              // starting point
     qreal x = 0, y = 0;                // current point
     char lastMode = 0;
@@ -1634,7 +1635,8 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
     const QChar *str = dataStr.constData();
     const QChar *end = str + dataStr.size();
 
-    while (str != end) {
+    bool ok = true;
+    while (ok && str != end) {
         while (str->isSpace() && (str + 1) != end)
             ++str;
         QChar pathElem = *str;
@@ -1651,14 +1653,13 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             arg.append(0);//dummy
         const qreal *num = arg.constData();
         int count = arg.count();
-        while (count > 0) {
+        while (ok && count > 0) {
             qreal offsetX = x;        // correction offsets
             qreal offsetY = y;        // for relative commands
             switch (pathElem.unicode()) {
             case 'm': {
                 if (count < 2) {
-                    num++;
-                    count--;
+                    ok = false;
                     break;
                 }
                 x = x0 = num[0] + offsetX;
@@ -1675,8 +1676,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
                 break;
             case 'M': {
                 if (count < 2) {
-                    num++;
-                    count--;
+                    ok = false;
                     break;
                 }
                 x = x0 = num[0];
@@ -1702,8 +1702,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
                 break;
             case 'l': {
                 if (count < 2) {
-                    num++;
-                    count--;
+                    ok = false;
                     break;
                 }
                 x = num[0] + offsetX;
@@ -1716,8 +1715,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
                 break;
             case 'L': {
                 if (count < 2) {
-                    num++;
-                    count--;
+                    ok = false;
                     break;
                 }
                 x = num[0];
@@ -1757,8 +1755,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
                 break;
             case 'c': {
                 if (count < 6) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF c1(num[0] + offsetX, num[1] + offsetY);
@@ -1774,8 +1771,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 'C': {
                 if (count < 6) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF c1(num[0], num[1]);
@@ -1791,8 +1787,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 's': {
                 if (count < 4) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF c1;
@@ -1813,8 +1808,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 'S': {
                 if (count < 4) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF c1;
@@ -1835,8 +1829,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 'q': {
                 if (count < 4) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF c(num[0] + offsetX, num[1] + offsetY);
@@ -1851,8 +1844,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 'Q': {
                 if (count < 4) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF c(num[0], num[1]);
@@ -1867,8 +1859,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 't': {
                 if (count < 2) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF e(num[0] + offsetX, num[1] + offsetY);
@@ -1888,8 +1879,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 'T': {
                 if (count < 2) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 QPointF e(num[0], num[1]);
@@ -1909,8 +1899,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
             case 'a': {
                 if (count < 7) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 qreal rx = (*num++);
@@ -1932,8 +1921,7 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
                 break;
             case 'A': {
                 if (count < 7) {
-                    num += count;
-                    count = 0;
+                    ok = false;
                     break;
                 }
                 qreal rx = (*num++);
@@ -1954,12 +1942,15 @@ static bool parsePathDataFast(const QStringRef &dataStr, QPainterPath &path)
             }
                 break;
             default:
-                return false;
+                ok = false;
+                break;
             }
             lastMode = pathElem.toLatin1();
+            if (path.elementCount() > maxElementCount)
+                ok = false;
         }
     }
-    return true;
+    return ok;
 }
 
 static bool parseStyle(QSvgNode *node,
@@ -2997,8 +2988,8 @@ static QSvgNode *createPathNode(QSvgNode *parent,
 
     QPainterPath qpath;
     qpath.setFillRule(Qt::WindingFill);
-    //XXX do error handling
-    parsePathDataFast(data, qpath);
+    if (!parsePathDataFast(data, qpath))
+        qCWarning(lcSvgHandler, "Invalid path data; path truncated.");
 
     QSvgNode *path = new QSvgPath(parent, qpath);
     return path;
-- 
2.36.0

From 41ab201cd44e4f0c7f6d22fca862d5650bf574ee Mon Sep 17 00:00:00 2001
From: Eirik Aavitsland <eirik.aavitsland@qt.io>
Date: Mon, 25 Oct 2021 14:43:09 +0200
Subject: [PATCH 09/10] SVG Image reading: Reject oversize svgs as corrupt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Add an upper limit for height and width at 0xffff, same as jpeg.

Fixes: QTBUG-95891
Pick-to: 6.2 5.15 5.12
Change-Id: I0dbc80dab3aab9b4743548772fb63fa69ea21f8a
Reviewed-by: Robert Löhning <robert.loehning@qt.io>
Reviewed-by: Allan Sandfeld Jensen <allan.jensen@qt.io>
(cherry picked from commit e544d8e457d52b543cae5c988f81237c7d6608da)

asturmlechner 2022-01-03: resolve conflict with preceding dev branch
  commit 0003ec68e9925a8386eb055e0030fe7f270aa56f.
---
 src/plugins/imageformats/svg/qsvgiohandler.cpp | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/src/plugins/imageformats/svg/qsvgiohandler.cpp b/src/plugins/imageformats/svg/qsvgiohandler.cpp
index 4136aaf..fd3529a 100644
--- a/src/plugins/imageformats/svg/qsvgiohandler.cpp
+++ b/src/plugins/imageformats/svg/qsvgiohandler.cpp
@@ -189,6 +189,8 @@ bool QSvgIOHandler::read(QImage *image)
             }
         }
         if (!finalSize.isEmpty()) {
+            if (qMax(finalSize.width(), finalSize.height()) > 0xffff)
+                return false; // Assume corrupted file
             image->fill(d->backColor.rgba());
             QPainter p(image);
             d->r.render(&p, bounds);
-- 
2.36.0

From 23b8cf7d833c335d7735855570c05e9e0893a9b7 Mon Sep 17 00:00:00 2001
From: Eirik Aavitsland <eirik.aavitsland@qt.io>
Date: Wed, 5 Jan 2022 09:48:22 +0100
Subject: [PATCH 10/10] Unconditionally stop parsing after the svg end tag
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

A QSvghandler may be created either with an external QXmlStreamReader
object, or with a bytearray/iodevice, in which case it will create its
own stream reader. The check to end parisng at the </svg> tag was
active only in the first case. This could result in different behavior
when reading an svg image from file vs. reading it from a bytearray
or resource.

Fixes: QTBUG-99407
Pick-to: 6.3 6.2 5.15
Change-Id: I187b39256f2b16ea952a3ae1b77c067ff96e4155
Reviewed-by: Allan Sandfeld Jensen <allan.jensen@qt.io>
Reviewed-by: Robert Löhning <robert.loehning@qt.io>
(cherry picked from commit 1749388cdc765fca4206aaf0f84ac9b0877dfc9a)
---
 src/svg/qsvghandler.cpp | 4 +---
 1 file changed, 1 insertion(+), 3 deletions(-)

diff --git a/src/svg/qsvghandler.cpp b/src/svg/qsvghandler.cpp
index 2ea80ed..f2653af 100644
--- a/src/svg/qsvghandler.cpp
+++ b/src/svg/qsvghandler.cpp
@@ -3696,9 +3696,7 @@ void QSvgHandler::parse()
         case QXmlStreamReader::EndElement:
             endElement(xml->name());
             ++remainingUnfinishedElements;
-            // if we are using somebody else's qxmlstreamreader
-            // we should not read until the end of the stream
-            done = !m_ownsReader && (xml->name() == QLatin1String("svg"));
+            done = (xml->name() == QLatin1String("svg"));
             break;
         case QXmlStreamReader::Characters:
             characters(xml->text());
-- 
2.36.0

