# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdiff3
pkgver=1.9.5
pkgrel=0
pkgdesc="Diff/Patch frontend for KDE"
url="https://kde.org/applications/development/org.kde.kdiff3"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdoctools-dev ki18n-dev kiconthemes-dev kitemviews-dev kjobwidgets-dev
	kparts-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev solid-dev sonnet-dev boost-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kdiff3/kdiff3-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	case $CTARGET_ARCH in
	ppc) export LDFLAGS="$LDFLAGS -Wl,--no-as-needed -latomic -Wl,--as-needed";;
	esac

	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4c9629b2a8bc506a5b224a566cf2ef9b733d78fc3185d69988ed387621039a3f91472b02344377a254b3f19771bea5ce3355b0fd866402594eb63fd940a68c41  kdiff3-1.9.5.tar.xz"
