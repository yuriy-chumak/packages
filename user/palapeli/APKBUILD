# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=palapeli
pkgver=22.04.2
pkgrel=0
pkgdesc="Jigsaw puzzle game by KDE"
url="https://www.kde.org/applications/games/palapeli/"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	karchive-dev kcompletion-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kcrash-dev ki18n-dev kio-dev kitemviews-dev
	knotifications-dev kservice-dev kwidgetsaddons-dev kxmlgui-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/palapeli-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d4e115250e7833283c059d5db007065e75d9a257e0fdabcbc5a5586ccac6c6b7813d5902b5d9387a57eac5f8966437c06a3ef821114f8c466371e6460f48c059  palapeli-22.04.2.tar.xz"
