# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kio
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for data and file management"
url="https://www.kde.org/"
arch="all"
options="!checkroot !check"  # Test requires X11; segfault bug #34
license="LGPL-2.1-only AND LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="kded"
depends_dev="qt5-qtbase-dev attica-dev kbookmarks-dev kcompletion-dev
	kconfig-dev kcoreaddons-dev kiconthemes-dev kitemviews-dev
	kjobwidgets-dev kservice-dev kwidgetsaddons-dev solid-dev"
docdepends="kcoreaddons-doc kservice-doc kcompletion-doc kwidgetsaddons-doc
	kjobwidgets-doc kbookmarks-doc kitemviews-doc kxmlgui-doc solid-doc
	kwindowsystem-doc kconfig-doc kconfigwidgets-doc kcodecs-doc kauth-doc"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev python3
	qt5-qttools-dev doxygen graphviz karchive-dev kdbusaddons-dev ki18n-dev
	knotifications-dev kwallet-dev acl-dev kded-dev krb5-dev $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kio-$pkgver.tar.xz
	cxx17.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="acf7456ab0a3d122d8bbf6e7dbff51f958b1c6bf29f7295c21c78c5c707d5fee05b76d30e51832f35220cd08bf06a2e27c11153559b25f74b7c8f5f532f50955  kio-5.94.0.tar.xz
a3be4ad4800ada7331a47833f51d13bc5cf47e9e6153fffff27d18462f77083221a7aa02a36c0afe57d992a5292777628c09c5443ff58ca1803ddfbc482fa745  cxx17.patch"
