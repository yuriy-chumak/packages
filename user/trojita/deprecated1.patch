From 297f0d5a1df9c1b9ca5e9287bb2e603985c232cf Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Tue, 1 Feb 2022 17:06:49 +0100
Subject: [PATCH 1/6] Port away from deprecated qSort to std::sort

---
 src/Imap/Model/MailboxTree.cpp                   | 2 +-
 src/Imap/Model/Model.cpp                         | 7 +++----
 src/Imap/Model/ThreadingMsgListModel.cpp         | 2 +-
 src/Imap/Parser/Sequence.cpp                     | 3 ++-
 src/Imap/Tasks/ObtainSynchronizedMailboxTask.cpp | 2 +-
 src/Imap/Tasks/SortTask.cpp                      | 2 +-
 6 files changed, 9 insertions(+), 9 deletions(-)

diff --git a/src/Imap/Model/MailboxTree.cpp b/src/Imap/Model/MailboxTree.cpp
index e61f59c5..d9a03fc3 100644
--- a/src/Imap/Model/MailboxTree.cpp
+++ b/src/Imap/Model/MailboxTree.cpp
@@ -590,7 +590,7 @@ void TreeItemMailbox::handleVanished(Model *const model, const Responses::Vanish
     QModelIndex listIndex = list->toIndex(model);
 
     auto uids = resp.uids;
-    qSort(uids);
+    std::sort(uids.begin(), uids.end());
     // Remove duplicates -- even that garbage can be present in a perfectly valid VANISHED :(
     uids.erase(std::unique(uids.begin(), uids.end()), uids.end());
 
diff --git a/src/Imap/Model/Model.cpp b/src/Imap/Model/Model.cpp
index 1d539e22..aff8ea43 100644
--- a/src/Imap/Model/Model.cpp
+++ b/src/Imap/Model/Model.cpp
@@ -25,7 +25,6 @@
 #include <QAuthenticator>
 #include <QCoreApplication>
 #include <QDebug>
-#include <QtAlgorithms>
 #include "Model.h"
 #include "Common/FindWithUnknown.h"
 #include "Common/InvokeMethod.h"
@@ -350,7 +349,7 @@ void Model::finalizeList(Parser *parser, TreeItemMailbox *mailboxPtr)
             ++it;
         }
     }
-    qSort(mailboxes.begin(), mailboxes.end(), MailboxNameComparator);
+    std::sort(mailboxes.begin(), mailboxes.end(), MailboxNameComparator);
 
     // Remove duplicates; would be great if this could be done in a STLish way,
     // but unfortunately std::unique won't help here (the "duped" part of the
@@ -406,7 +405,7 @@ void Model::finalizeIncrementalList(Parser *parser, const QString &parentMailbox
             ++it;
         }
     }
-    qSort(mailboxes.begin(), mailboxes.end(), MailboxNameComparator);
+    std::sort(mailboxes.begin(), mailboxes.end(), MailboxNameComparator);
 
     if (mailboxes.size() == 0) {
         qDebug() << "Weird, no matching LIST response for our prompt after CREATE";
@@ -1259,7 +1258,7 @@ void Model::copyMoveMessages(TreeItemMailbox *sourceMbox, const QString &destMai
 
     Q_ASSERT(sourceMbox);
 
-    qSort(uids);
+    std::sort(uids.begin(), uids.end());
 
     QModelIndexList messages;
     Sequence seq;
diff --git a/src/Imap/Model/ThreadingMsgListModel.cpp b/src/Imap/Model/ThreadingMsgListModel.cpp
index 29786022..76ef94c2 100644
--- a/src/Imap/Model/ThreadingMsgListModel.cpp
+++ b/src/Imap/Model/ThreadingMsgListModel.cpp
@@ -718,7 +718,7 @@ void ThreadingMsgListModel::slotIncrementalThreadingAvailable(const Responses::E
     for (Responses::ESearch::IncrementalThreadingData_t::const_iterator it = data.constBegin(); it != data.constEnd(); ++it) {
         gatherAllUidsFromThreadNode(affectedUids, it->thread);
     }
-    qSort(affectedUids);
+    std::sort(affectedUids.begin(), affectedUids.end());
     QList<TreeItemMessage*> affectedMessages = const_cast<Model*>(realModel)->
             findMessagesByUids(static_cast<TreeItemMailbox*>(mailboxIndex.internalPointer()), affectedUids);
     QHash<uint,void *> uidToPtrCache;
diff --git a/src/Imap/Parser/Sequence.cpp b/src/Imap/Parser/Sequence.cpp
index 31b257bb..0e2cd314 100644
--- a/src/Imap/Parser/Sequence.cpp
+++ b/src/Imap/Parser/Sequence.cpp
@@ -23,6 +23,7 @@
 #include "Sequence.h"
 #include <QStringList>
 #include <QTextStream>
+#include <algorithm>
 
 namespace Imap
 {
@@ -114,7 +115,7 @@ Sequence &Sequence::add(uint num)
 Sequence Sequence::fromVector(Imap::Uids numbers)
 {
     Q_ASSERT(!numbers.isEmpty());
-    qSort(numbers);
+    std::sort(numbers.begin(), numbers.end());
     Sequence seq(numbers.first());
     for (int i = 1; i < numbers.size(); ++i) {
         seq.add(numbers[i]);
diff --git a/src/Imap/Tasks/ObtainSynchronizedMailboxTask.cpp b/src/Imap/Tasks/ObtainSynchronizedMailboxTask.cpp
index 9d6f8279..89fdf4ef 100644
--- a/src/Imap/Tasks/ObtainSynchronizedMailboxTask.cpp
+++ b/src/Imap/Tasks/ObtainSynchronizedMailboxTask.cpp
@@ -974,7 +974,7 @@ void ObtainSynchronizedMailboxTask::finalizeSearch()
     }
     }
 
-    qSort(uidMap);
+    std::sort(uidMap.begin(), uidMap.end());
     if (!uidMap.isEmpty() && uidMap.front() == 0) {
         throw MailboxException("UID (E)SEARCH response contains invalid UID zero");
     }
diff --git a/src/Imap/Tasks/SortTask.cpp b/src/Imap/Tasks/SortTask.cpp
index 347ec85e..8c2b4d69 100644
--- a/src/Imap/Tasks/SortTask.cpp
+++ b/src/Imap/Tasks/SortTask.cpp
@@ -179,7 +179,7 @@ bool SortTask::handleSearch(const Imap::Responses::Search *const resp)
     // That just doesn't look like worth it.
 
     sortResult += resp->items;
-    qSort(sortResult);
+    std::sort(sortResult.begin(), sortResult.end());
     sortResult.erase(std::unique(sortResult.begin(), sortResult.end()), sortResult.end());
     return true;
 }
-- 
GitLab


From 268637f7b63059a065dffcb87c13166ebb0d3d3f Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Tue, 1 Feb 2022 17:31:43 +0100
Subject: [PATCH 2/6] Port away from deprecated qFind to std::find

---
 src/Imap/Model/ThreadingMsgListModel.cpp | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/src/Imap/Model/ThreadingMsgListModel.cpp b/src/Imap/Model/ThreadingMsgListModel.cpp
index 76ef94c2..58b606a5 100644
--- a/src/Imap/Model/ThreadingMsgListModel.cpp
+++ b/src/Imap/Model/ThreadingMsgListModel.cpp
@@ -1130,7 +1130,7 @@ void ThreadingMsgListModel::pruneTree()
             Q_ASSERT(parent != threading.end());
 
             // and the node itself has to be found in its parent's children
-            QList<uint>::iterator childIt = qFind(parent->children.begin(), parent->children.end(), it->internalId);
+            QList<uint>::iterator childIt = std::find(parent->children.begin(), parent->children.end(), it->internalId);
             Q_ASSERT(childIt != parent->children.end());
             // The offset of this child might no longer be correct, though -- we're postponing the actual deletion until later
 
@@ -1175,7 +1175,7 @@ void ThreadingMsgListModel::pruneTree()
 
                 if (parent->internalId == 0) {
                     // Update the list of all thread roots
-                    QList<uint>::iterator rootIt = qFind(threadedRootIds.begin(), threadedRootIds.end(), it->internalId);
+                    QList<uint>::iterator rootIt = std::find(threadedRootIds.begin(), threadedRootIds.end(), it->internalId);
                     if (rootIt != threadedRootIds.end())
                         *rootIt = replaceWith->internalId;
                 }
-- 
GitLab


From f06067dfa9bbb28cc385e526e9a7b261fe8448bc Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Tue, 1 Feb 2022 17:33:32 +0100
Subject: [PATCH 3/6] Port away from deprecated qLowerBound to std::lower_bound

---
 src/Imap/Parser/Sequence.cpp | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/src/Imap/Parser/Sequence.cpp b/src/Imap/Parser/Sequence.cpp
index 0e2cd314..55adb6aa 100644
--- a/src/Imap/Parser/Sequence.cpp
+++ b/src/Imap/Parser/Sequence.cpp
@@ -106,7 +106,7 @@ Imap::Uids Sequence::toVector() const
 Sequence &Sequence::add(uint num)
 {
     Q_ASSERT(kind == DISTINCT);
-    auto it = qLowerBound(numbers.begin(), numbers.end(), num);
+    auto it = std::lower_bound(numbers.begin(), numbers.end(), num);
     if (it == numbers.end() || *it != num)
         numbers.insert(it, num);
     return *this;
-- 
GitLab


From b7e9532a3209d64c15937e29b359be40fbd2fff7 Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Tue, 1 Feb 2022 17:34:15 +0100
Subject: [PATCH 4/6] Port away from deprecated qLess to std::less

---
 tests/Misc/test_algorithms.cpp | 5 +++--
 1 file changed, 3 insertions(+), 2 deletions(-)

diff --git a/tests/Misc/test_algorithms.cpp b/tests/Misc/test_algorithms.cpp
index a9a53745..86ce9e16 100644
--- a/tests/Misc/test_algorithms.cpp
+++ b/tests/Misc/test_algorithms.cpp
@@ -21,6 +21,7 @@
 */
 
 #include <QTest>
+#include <functional>
 #include "test_algorithms.h"
 
 #include "Common/FindWithUnknown.h"
@@ -38,9 +39,9 @@ void TestCommonAlgorithms::testLowerBoundWithUnknown()
     QFETCH(int, needle);
     QFETCH(int, offset);
 
-    QList<int>::const_iterator it = Common::linearLowerBoundWithUnknownElements(list.constBegin(), list.constEnd(), needle, isZero, qLess<int>());
+    QList<int>::const_iterator it = Common::linearLowerBoundWithUnknownElements(list.constBegin(), list.constEnd(), needle, isZero, std::less<int>());
     QCOMPARE(it - list.constBegin(), offset);
-    it = Common::lowerBoundWithUnknownElements(list.constBegin(), list.constEnd(), needle, isZero, qLess<int>());
+    it = Common::lowerBoundWithUnknownElements(list.constBegin(), list.constEnd(), needle, isZero, std::less<int>());
     QCOMPARE(it - list.constBegin(), offset);
 }
 
-- 
GitLab


From 42ea2f0ec8de8fc2ea3e607d0b694dbd30e3ffaf Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Sat, 29 Jan 2022 23:54:40 +0100
Subject: [PATCH 5/6] Port from the deprecated QProcess::error() to
 errorOccurred

Raises the Qt requirement to 5.6.
---
 CMakeLists.txt                 | 3 ++-
 src/MSA/Sendmail.cpp           | 2 +-
 src/Streams/IODeviceSocket.cpp | 2 +-
 3 files changed, 4 insertions(+), 3 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index a3d54166..54512029 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -69,7 +69,7 @@ else()
     set(QTKEYCHAIN_DEPENDS "")
 endif()
 
-find_package(Qt5Core 5.2 REQUIRED)
+find_package(Qt5Core 5.6 REQUIRED)
 find_package(Qt5Gui REQUIRED)
 find_package(Qt5Network REQUIRED)
 find_package(Qt5Sql REQUIRED)
@@ -183,6 +183,7 @@ add_definitions(-DQT_STRICT_ITERATORS)
 add_definitions(-DQT_USE_QSTRINGBUILDER)
 add_definitions(-DQT_USE_FAST_OPERATOR_PLUS)
 add_definitions(-DQT_USE_FAST_CONCATENATION)
+add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x050600)
 
 if(NOT MSVC)
   # We're using C++11's threading features (std::async in particular), and that requires "some threading". With GCC and
diff --git a/src/MSA/Sendmail.cpp b/src/MSA/Sendmail.cpp
index f72f4927..d88d16c4 100644
--- a/src/MSA/Sendmail.cpp
+++ b/src/MSA/Sendmail.cpp
@@ -30,7 +30,7 @@ Sendmail::Sendmail(QObject *parent, const QString &command, const QStringList &a
     proc = new QProcess(this);
     connect(proc, &QProcess::started, this, &Sendmail::handleStarted);
     connect(proc, static_cast<void (QProcess::*)(int)>(&QProcess::finished), this, &Sendmail::handleFinished);
-    connect(proc, static_cast<void (QProcess::*)(QProcess::ProcessError)>(&QProcess::error), this, &Sendmail::handleError);
+    connect(proc, &QProcess::errorOccurred, this, &Sendmail::handleError);
     connect(proc, &QIODevice::bytesWritten, this, &Sendmail::handleBytesWritten);
 }
 
diff --git a/src/Streams/IODeviceSocket.cpp b/src/Streams/IODeviceSocket.cpp
index 7998028d..90f8470e 100644
--- a/src/Streams/IODeviceSocket.cpp
+++ b/src/Streams/IODeviceSocket.cpp
@@ -141,7 +141,7 @@ ProcessSocket::ProcessSocket(QProcess *proc, const QString &executable, const QS
     IODeviceSocket(proc), executable(executable), args(args)
 {
     connect(proc, &QProcess::stateChanged, this, &ProcessSocket::handleStateChanged);
-    connect(proc, static_cast<void (QProcess::*)(QProcess::ProcessError)>(&QProcess::error), this, &ProcessSocket::handleProcessError);
+    connect(proc, &QProcess::errorOccurred, this, &ProcessSocket::handleProcessError);
 }
 
 ProcessSocket::~ProcessSocket()
-- 
GitLab


From aee63baa14f3fc318d4aac67b2c740067ffc7929 Mon Sep 17 00:00:00 2001
From: Heiko Becker <heirecka@exherbo.org>
Date: Tue, 1 Feb 2022 17:52:12 +0100
Subject: [PATCH 6/6] Port from deprecated {from,to}Time_t to
 {from,to}SecsSinceEpock

Raises the Qt requirement to 5.8.

Not raising QT_DISABLE_DEPRECATED_BEFORE=0x050800 yet, because
QModelIndex.child() was deprecated with 5.8 and it's quite frequenty
used.
---
 CMakeLists.txt               | 4 ++--
 src/Cryptography/GpgMe++.cpp | 4 ++--
 src/Imap/Model/Utils.cpp     | 2 +-
 3 files changed, 5 insertions(+), 5 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 54512029..e2cd9108 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -69,7 +69,7 @@ else()
     set(QTKEYCHAIN_DEPENDS "")
 endif()
 
-find_package(Qt5Core 5.6 REQUIRED)
+find_package(Qt5Core 5.8 REQUIRED)
 find_package(Qt5Gui REQUIRED)
 find_package(Qt5Network REQUIRED)
 find_package(Qt5Sql REQUIRED)
@@ -183,7 +183,7 @@ add_definitions(-DQT_STRICT_ITERATORS)
 add_definitions(-DQT_USE_QSTRINGBUILDER)
 add_definitions(-DQT_USE_FAST_OPERATOR_PLUS)
 add_definitions(-DQT_USE_FAST_CONCATENATION)
-add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x050600)
+add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x050700)
 
 if(NOT MSVC)
   # We're using C++11's threading features (std::async in particular), and that requires "some threading". With GCC and
diff --git a/src/Cryptography/GpgMe++.cpp b/src/Cryptography/GpgMe++.cpp
index 716b8aff..78f8caf5 100644
--- a/src/Cryptography/GpgMe++.cpp
+++ b/src/Cryptography/GpgMe++.cpp
@@ -366,7 +366,7 @@ void GpgMePart::extractSignatureStatus(std::shared_ptr<GpgME::Context> ctx, cons
     } else {
         signer = QString::fromUtf8(sig.fingerprint());
     }
-    signDate = QDateTime::fromTime_t(sig.creationTime());
+    signDate = QDateTime::fromSecsSinceEpoch(sig.creationTime());
 
     if (sig.summary() & GpgME::Signature::Green) {
         // FIXME: change the above to GpgME::Signature::Valid and react to expired keys/signatures by checking the timestamp
@@ -462,7 +462,7 @@ void GpgMePart::extractSignatureStatus(std::shared_ptr<GpgME::Context> ctx, cons
     if (sig.summary() & GpgME::Signature::SigExpired) {
         ENSURE_LINE_LF(longStatus);
         longStatus += tr("Signature expired on %1.")
-                .arg(QDateTime::fromTime_t(sig.expirationTime()).toString(Qt::DefaultLocaleShortDate));
+                .arg(QDateTime::fromSecsSinceEpoch(sig.expirationTime()).toString(Qt::DefaultLocaleShortDate));
     }
     if (sig.summary() & GpgME::Signature::KeyMissing) {
         ENSURE_LINE_LF(longStatus);
diff --git a/src/Imap/Model/Utils.cpp b/src/Imap/Model/Utils.cpp
index 00b3d4b3..b09341a6 100644
--- a/src/Imap/Model/Utils.cpp
+++ b/src/Imap/Model/Utils.cpp
@@ -343,7 +343,7 @@ QString formatDateTimeWithTimeZoneAtEnd(const QDateTime &now, const QString &for
 
     // Got to cast to a signed type to prevent unsigned underflow here. Also go to 64bits because otherwise there'd
     // a problem when the value is out-of-range for an int32.
-    int minutesDifference = (static_cast<qint64>(now.toTime_t()) - static_cast<qint64>(nowUtc.toTime_t())) / 60;
+    int minutesDifference = (now.toSecsSinceEpoch() - nowUtc.toSecsSinceEpoch()) / 60;
     int tzOffsetHours = qAbs(minutesDifference) / 60;
     int tzOffsetMinutes = qAbs(minutesDifference) % 60;
     // The rest is just a piece of cake now
-- 
GitLab

