# Contributor: Dan Theisen <djt@hxx.in>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=olm
pkgver=3.2.16
pkgrel=0
pkgdesc="Implementation of the olm and megolm cryptographic ratchets"
url="https://gitlab.matrix.org/matrix-org/olm/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="cmake"
subpackages="$pkgname-dev"
source="https://gitlab.matrix.org/matrix-org/olm/-/archive/$pkgver/olm-$pkgver.tar.bz2"

# secfixes:
#   3.2.6-r0:
#     - CVE-2021-34813

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="504ddc91297e7aef99e9b25f73b33bac29716ca33eb333bf1217b719d8862438e2cdaadd88cef3dc84e9f618bcc9eeeaf7e513f2d6909cc4a3d98a5dd79b0581  olm-3.2.16.tar.bz2"
