# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=graphviz
pkgver=2.50.0
pkgrel=0
pkgdesc="Graph visualization software"
url="http://www.graphviz.org/"
arch="all"
options="!check"  # Requires unpackaged Criterion test framework
license="EPL"
depends=""
depends_dev="zlib-dev libpng-dev libjpeg-turbo-dev expat-dev
	fontconfig-dev libsm-dev libxext-dev cairo-dev pango-dev
	librsvg-dev gmp-dev freetype-dev"
makedepends="$depends_dev flex swig m4 libtool guile-dev
	bison gtk+2.0-dev libltdl perl-dev python3-dev tcl tcl-dev"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/graphviz"
subpackages="$pkgname-dev $pkgname-doc guile-$pkgname:guile
	$pkgname-gtk $pkgname-graphs perl-gv:_pl py3-gv:_py3 tcl-$pkgname:_tcl"
source="https://gitlab.com/api/v4/projects/4207231/packages/generic/$pkgname-releases/$pkgver/$pkgname-$pkgver.tar.xz
	$pkgname.trigger
	0001-clone-nameclash.patch
	"

build() {
	LIBPOSTFIX=/ \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--without-included-ltdl \
		--disable-ltdl-install \
		--disable-silent-rules \
		--enable-ltdl \
		--with-x \
		--disable-static \
		--disable-dependency-tracking \
		--enable-java=no \
		--enable-tcl=yes \
		--without-mylibgd \
		--with-ipsepcola \
		--with-pangocairo \
		--with-gdk-pixbuf \
		--with-png \
		--with-jpeg \
		--with-rsvg

	if [ "$CARCH" = "x86_64" ]; then
		# the configure script thinks we have sincos. we dont.
		sed -i -e '/HAVE_SINCOS/d' config.h
	fi

	make
}

package() {
	make DESTDIR="$pkgdir" \
		pkgconfigdir=/usr/lib/pkgconfig \
		install

	mkdir -p "$pkgdir"/usr/share/doc
	mv "$pkgdir"/usr/share/graphviz/doc \
		"$pkgdir"/usr/share/doc/graphviz
}

_lang() {
	pkgdesc="$2 bindings for graphviz"
	depends="$3"
	mkdir -p "$subpkgdir"/usr/lib/graphviz
	mv "$pkgdir"/usr/lib/graphviz/$1* \
		"$subpkgdir"/usr/lib/graphviz/
	mv "$pkgdir"/usr/lib/$1* "$subpkgdir"/usr/lib/ || true
}

guile() {
	_lang guile Guile guile
}

gtk() {
	pkgdesc="Gtk extension for graphviz"
	mkdir -p "$subpkgdir"/usr/lib/graphviz
	mv "$pkgdir"/usr/lib/graphviz/libgvplugin_g?k* \
		"$pkgdir"/usr/lib/graphviz/libgvplugin_rsvg* \
		"$subpkgdir"/usr/lib/graphviz
}

graphs() {
	pkgdesc="Demo graphs for graphviz"
	mkdir -p "$subpkgdir"/usr/share/graphviz
	mv "$pkgdir"/usr/share/graphviz/graphs \
		"$subpkgdir"/usr/share/graphviz/
}

_pl() {
	_lang perl Perl
}

_py3() {
	_lang python3 "Python 3"
}

_tcl() {
	_lang tcl "Tcl/Tk"
}

sha512sums="2b035559da20bad35e046bfa1b2c8ce1b395ff9b812f33bcf612d7f7c65ff9a226c9b209553b4283825330683fb925516563943de7922c2f6434baaf3c3b5ee2  graphviz-2.50.0.tar.xz
50947e6a11929f724759266f7716d52d10923eba6d59704ab39e4bdf18f8471d548c2b11ab051dd4b67cb82742aaf54d6358890d049d5b5982f3383b65f7ae8c  graphviz.trigger
6c749a15fb7e52107d74bd49dc49f11b7187bf088f0d73c57f08777032c2a55f49b6021aa40fdf89fd9dbd3c9d02ca45723b225d001117fd462b1e148cb3f44a  0001-clone-nameclash.patch"
