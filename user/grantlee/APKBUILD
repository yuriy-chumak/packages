# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grantlee
pkgver=5.3.1
pkgrel=0
pkgdesc="Qt string template engine based on the Django template system"
url="https://github.com/steveire/grantlee/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="cmake $depends_dev qt5-qtscript-dev doxygen graphviz
	qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/steveire/$pkgname/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz
	x87fpu.patch
	enum-comparators.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make all docs
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E buildertest
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc/grantlee
	mv "$builddir"/apidox "$pkgdir"/usr/share/doc/grantlee/
}

sha512sums="dc7192fe0553954fffc3e2c584e4fdd80fc1f22d25846cacc5f2dcd1db2673ca62464c8492a4ed3bfc9dfc3e62ef13322809dd29bd56fa4a3a153a8d373ddde5  grantlee-5.3.1.tar.gz
b515403727416ef91037e5774a78e4b477e4e99da0c58e53f3099a6f8558c25244416acabd2bde529aafa509c4a61c54761d39fdaceb756bc4b638ccc3285607  x87fpu.patch
b5c57a73786978a61c1a1164aefa171a23fbb033a9fc980cceb9ab6cc48f90cc86be15a367cbc39e6234c261a3113d72189a02e16affac428ec4a8de67377fc9  enum-comparators.patch"
