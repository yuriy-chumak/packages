# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kscreen
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE Plasma screen management software (user-facing)"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires dbus-x11 and both of them running
license="LGPL-2.1+ AND GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only)"
depends=""
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtx11extras-dev
	qt5-qtsensors-dev libxcb-dev kcmutils-dev kconfig-dev kdbusaddons-dev
	kdeclarative-dev kglobalaccel-dev ki18n-dev kiconthemes-dev kxmlgui-dev
	plasma-framework-dev libkscreen-dev libxi-dev kauth-dev kcodecs-dev
	kcoreaddons-dev kpackage-dev kservice-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kscreen-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="770dfb968f8f8bf96b6ad3bf71f18f5baca35b2a4daa72b8806957ad1e050003158319edba84ebf999134e106ef916d829d391c33d79c58edb1000f503f67c97  kscreen-5.24.5.tar.xz"
