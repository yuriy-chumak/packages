# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=r
pkgver=4.2.3
pkgrel=0
pkgdesc="Environment for statistical computing and graphics"
url="https://www.r-project.org/"
arch="all"
options="!check"  # 20.482886 != 20.482887
license="GPL-2.0-only"
depends="cmd:which"
makedepends="byacc bzip2-dev cairo-dev curl-dev gfortran icu-dev libice-dev
	libtirpc-dev libx11-dev libxt-dev pango-dev pcre-dev pcre2-dev xz-dev
	zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://cran.r-project.org/src/base/R-${pkgver%%.*}/R-$pkgver.tar.gz
	curl-8-support.patch
	"
builddir="$srcdir/R-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--docdir=/usr/share/doc/R-$pkgver \
		--localstatedir=/var \
		--with-readline=no \
		--enable-R-shlib
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="060bb4e1d1f1a5a0383a3b4372ac9247c0a20285020da17cebeb40ddc54da12d5f369ea243ea04d2c6970986fe22b3f9c37fbdfb3405cd8aa4f2353091ea9c5c  R-4.2.3.tar.gz
051dcaae4f082e8e6138a748dcf880aa0d6f8ee371025f3b09068f21530be36271a69cf2f5973c2f0832c4c865b9425c62bbeb59a2243a8ddff05969a743ddf8  curl-8-support.patch"
