# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=qtkeychain
pkgver=0.12.0
pkgrel=0
pkgdesc="Platform-independent Qt-based API for storing passwords securely"
url="https://github.com/frankosterfeld/qtkeychain"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev"
subpackages="$pkgname-dev"
source="qtkeychain-$pkgver.tar.gz::https://github.com/frankosterfeld/qtkeychain/archive/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ad8f7b3e8f59894a09892aeb78118f5ed93aa4593eece782c1879a4f3c37d9d63e8d40ad4b2e6a2e286e0da39f45cd4ed46181a1a05c078a59134114b2456a03  qtkeychain-0.12.0.tar.gz"
