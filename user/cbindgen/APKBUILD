# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Gentoo Rust Maintainers <rust@gentoo.org>
# Contributor: Samuel Holland <samuel@sholland.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=cbindgen
pkgver=0.26.0
pkgrel=0
pkgdesc="Tool to generate C bindings from Rust code"
url="https://github.com/eqrion/cbindgen"
arch="all"
license="MPL-2.0"
depends=""
makedepends="cargo"
checkdepends="py3-cython"
source=""

# dependencies taken from Cargo.lock
cargo_deps="$pkgname-$pkgver
atty-0.2.14
autocfg-1.1.0
bitflags-1.3.2
cfg-if-1.0.0
clap-3.2.25
clap_lex-0.2.4
fastrand-1.9.0
hashbrown-0.12.3
heck-0.4.1
hermit-abi-0.1.19
indexmap-1.9.3
instant-0.1.12
itoa-1.0.6
lazy_static-1.4.0
libc-0.2.144
lock_api-0.4.9
log-0.4.17
os_str_bytes-6.5.0
parking_lot-0.11.2
parking_lot_core-0.8.6
proc-macro2-1.0.66
quote-1.0.27
redox_syscall-0.2.16
remove_dir_all-0.5.3
ryu-1.0.13
scopeguard-1.1.0
serde-1.0.163
serde_derive-1.0.163
serde_json-1.0.96
serial_test-0.5.1
serial_test_derive-0.5.1
smallvec-1.10.0
strsim-0.10.0
syn-1.0.109
syn-2.0.16
tempfile-3.3.0
termcolor-1.2.0
textwrap-0.16.0
toml-0.5.11
unicode-ident-1.0.8
winapi-0.3.9
winapi-i686-pc-windows-gnu-0.4.0
winapi-util-0.1.5
winapi-x86_64-pc-windows-gnu-0.4.0
"

source="$source $(echo $cargo_deps | sed -E 's#([[:graph:]]+)-([0-9.]+(-(alpha|beta|rc)[0-9.]+)?)#&.tar.gz::https://crates.io/api/v1/crates/\1/\2/download#g')
	ppc-libc-hugetlb.patch
	"

prepare() {
	export CARGO_HOME="$srcdir/cargo-home"
	export CARGO_VENDOR="$CARGO_HOME/adelie"

	(builddir=$srcdir; default_prepare)

	mkdir -p "$CARGO_VENDOR"
	cat <<- EOF > "$CARGO_HOME/config"
		[source.adelie]
		directory = "${CARGO_VENDOR}"

		[source.crates-io]
		replace-with = "adelie"
		local-registry = "/nonexistant"
	EOF

	for _dep in $cargo_deps; do
		ln -s "$srcdir/$_dep" "$CARGO_VENDOR/$_dep"
		_sum=$(sha256sum "$srcdir/$_dep.tar.gz" | cut -d' ' -f1)
		cat <<- EOF > "$CARGO_VENDOR/$_dep/.cargo-checksum.json"
		{
			"package":"$_sum",
			"files":{}
		}
		EOF
	done
}

build() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo build -j $JOBS --release
}

check() {
	export CARGO_HOME="$srcdir/cargo-home"

#	# Failing tests
#	rm -rf tests/rust/expand*
	cargo test -j $JOBS --release
}

package() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo install --no-track --path . --root="$pkgdir"/usr
}


sha512sums="2de81f46c9c68c985241a349f13125876667d1460d3658ead8c4ee9788cd139c30dbc08bebddc172cf0bd4644f3f17c02cf66f2a3ef706c52366fdaf0f9d8059  cbindgen-0.26.0.tar.gz
d7b6c4b9a0f898d91ddbc41a5ee45bbf45d1d269508c8cc87ee3e3990500e41e0ec387afb1f3bc7db55bedac396dd86c6509f4bf9e5148d809c3802edcc5e1d9  atty-0.2.14.tar.gz
df972c09abbdc0b6cb6bb55b1e29c7fed706ece38a62613d9e275bac46a19574a7f96f0152cccb0239efea04ee90083a146b58b15307696c4c81878cd12de28f  autocfg-1.1.0.tar.gz
3c698f757b5cc62f815f9a1cce365c3d2dd88e4db71f331dff8bba86c2865f755b81cb4c9bfc59bd86b7643b0943f9e09a7c4f8ad75eb2ab0e714803d0129f62  bitflags-1.3.2.tar.gz
0fb16a8882fd30e86b62c5143b1cb18ab564e84e75bd1f28fd12f24ffdc4a42e0d2e012a99abb606c12efe3c11061ff5bf8e24ab053e550ae083f7d90f6576ff  cfg-if-1.0.0.tar.gz
557c8932175d7ecd077b32b68904924e52dd46d04fd04ba36b3a9dfd7ab1dbe8c2128fecfd75d0388b21fca4aee55a941794181cc2910a4d4eb3c54e9c7e73ea  clap-3.2.25.tar.gz
6c05e5fd850befd45be9005af7252385f2304aa28a107413bbe329d97aea835f7acfd0bd169c99f69f466ab93d6e1c35d73a4e48343457a06fe2d6be5bedde0f  clap_lex-0.2.4.tar.gz
321567b5fad8552c0efc4393b1e77d1bce288b0a88c475d432f79e91b3457ee6eb5db9e4d65ac6381b9990c9916f4651b6a76250df44d51ea3e25bd8184bdc52  fastrand-1.9.0.tar.gz
b3700fcd659a21a6b9b3777c18b37a83bf25542b4e8f2b963779a122f5d22e1742c064cfc03e649583e7dd5c6e90ca8407f8c51a0e8755f6a108682853022f76  hashbrown-0.12.3.tar.gz
8c80e959d2f10a2893f9a71994720f90747742bb5b61fc0a539eed3ea5679b140c48fd7f7690d7122cd6af5f7f20a19d412e3569fe741c6d31f6b2ce1e0b80e8  heck-0.4.1.tar.gz
1c877fcd562b15d2de9c151fd6c5f3ea4bf48abcb799e6139a180ffad5d64b632f0000d5707bbd92ff23a0e5f349157b9e0f5be8b50f03680b0fa47315dbb78a  hermit-abi-0.1.19.tar.gz
2aa8069eb07a814c8fa3e11296c9f032ef60963520d7786ad20cca5cb7e73b8f76d97722a994d65295bb713020aadce5008cd3df5e99d8bd968ef1979f910a37  indexmap-1.9.3.tar.gz
fae494c00111c51c840f9dd6a10febe403e27ebb933dd16633a213e9c20f2bc11adeb431c71f8a6713bf88f270a010941e15d83df294e658791934f83a5d2407  instant-0.1.12.tar.gz
e1fb82fe16e1248141d48de9e05e7abed0c6fef58f2ff8b77b52aca5f16f3600c46707ff4c7a0f0307047610f29775bda74948d6d1efceb74b37cdd22e1fcf31  itoa-1.0.6.tar.gz
e124c0521ec7c950f3c4a066821918da7a9c6e711115d98009ae7c351928fdddead852e7596fea5937a9c30e4e4ce8eee7099b20248b5d6e3b2494b6a6d88cb8  lazy_static-1.4.0.tar.gz
576da68e3845a7149f64bee425529ffec4bbb8df0272790182e49c8bad5b9744de21ffb5c1206753b57b7fe45af02c6c429e16522b72a77e5262482d64df5692  libc-0.2.144.tar.gz
9215381d9bb6b80d217c73a900db43df043b3e939b5bd7a292a02e9ab911cf0eacd8f883d35bdf72b3a0e78df8f1bc3e843ca4c775294c7a7a03091dc1a74990  lock_api-0.4.9.tar.gz
2477d88db42b1d92c30708d88823212e236f613b3465e85eb425f83f8d16fadfaf0352f06c2999a1852102edd2f6ffb10ecb539d8a3b6c48f552a25622ccffa2  log-0.4.17.tar.gz
cde7779ba24304256cc57de78208af4d26bc91dae7fbb9c85e3f67c3e35cb6268caf3fc9eef530a8d3cbdc951ec3534223f3559d2a013561912231ebdb1801b2  os_str_bytes-6.5.0.tar.gz
526b176363dffa59501c18324bb723a3846ef5b0ff9bf1d890e40ad10e7023284f7c8012eda87520eaa94515ee828d9ef52692a9ed590a55e176383d6d472f9e  parking_lot-0.11.2.tar.gz
906241f8e2d71784d572fb78978c9550b19af9c4e32fe3b2da751287806d0faeba61f5bd36f7aab026970b2bffaaa1f62ddc10c64dc348eae61bf7b51297ef80  parking_lot_core-0.8.6.tar.gz
85f5a762f9411142e5ac28144bd380f07f0633ed2e44d8a2545be9fb8f42abaca6b5d45631b4be83b8e8b9beca7438bc52f25615c3a410a3a1249474b1aca407  proc-macro2-1.0.66.tar.gz
5fb5802aa667c60d1a721766081b01bfd6cd929db7a5d71cb74627c2325f7108740752e514db73fb3612c163840e60ef2d4bde31c41978f3d77605418bdf2b4a  quote-1.0.27.tar.gz
63b5d876baaf99f5cf737679bc6ac7a9e3d8a41aa93f5c59416ce7e3841e2513bff678773553cfe62fb452707f82acc384ea63aec932a31bf94679cd1caddd27  redox_syscall-0.2.16.tar.gz
50417d6d8a33912193a1ed37eb72b47431b12ae65d2780cdb7080c3d141e63819da13751c3fb737685cea322f70b36d413389c3dc01aa12b4dce615aefed0e2c  remove_dir_all-0.5.3.tar.gz
25f60216d91e68cb47695ce4e966fae674d5b3e4b0cf33e740248c1605fdcf0c963acd278a485c5b4bb0a1c1144002e73173592222af4989df7a4ba402508c13  ryu-1.0.13.tar.gz
368fa5726df8f42b599993681579a9ffd0196480ee3cd0f9f671e8493f3bedd1e1779bdf2beb329e77e0005fa09b816e3385f309490c0f2781568db275d4d17d  scopeguard-1.1.0.tar.gz
752738ef5f67f1b4435bdd4e76e740ba192bd93e83cac55fd667809af26ceaf02db7083aaf7b2074644b38964021af206000b6130199e7994c12c9c57cd31a34  serde-1.0.163.tar.gz
744c4235330781c8d9c6ab4f49f6ddd11fcd70b97c97fcbf1434ccac02ba7d29eacfed21ee8e005f5541dd8ab4a34023e95d85231f11c5f4d6e24fcf806f7283  serde_derive-1.0.163.tar.gz
39779419900e1d395d81e70cff0e7a688564a66124b08e74da54ac30f389c912707f4bf5e29afab6fa106faf4bf4e0b841f42fef125cf7bec185482ff6bbba0e  serde_json-1.0.96.tar.gz
e1a0f7a24981698eaa6bcce8f951863f76e8a2750aff3191104d092a06021c39d4eb2e9b74e6690b0dba0d674a216ea170efe0a5367d22bdef72c2006f644a4e  serial_test-0.5.1.tar.gz
e3f4b3c2eed1b284dbff7447c2f912343f9b95cbd88f3387c0136ca42698b38a607c752277ee4590ded9f73f475325d2652ba67ba029ddd54711d9070ac5f43e  serial_test_derive-0.5.1.tar.gz
a09110184582dcc01d7a0d3fa8f74c17bf726935126d3654667b8e9c4bc43ad16ccfd8fa94feae7d9b31913aa7ee030fe5936e4b44a36302b6ce5fe37372a7ae  smallvec-1.10.0.tar.gz
78b318532addfcf5c1ccc1e14539e258aab9d3cd893cc45d82342549bde838c177d90f13c560671f8f32929af47d0b467db35e6876bd7697d8b3f9e055aeeac1  strsim-0.10.0.tar.gz
12816b9e8cf984024b2fbce9f0ae14cf94d4d2c06f08cc54fb793ce78770bb4cc1288eb7df0ba5e8e937756e1e8e295c53fe07a0c5dde1ea8ddba03b6203b37d  syn-1.0.109.tar.gz
75d6728614a39d7f676536fc345be6777e8dba6198f031e576ecd199e503347c4069fc7294c4da3e829cb919c08347513447dc86fbf0c545df2d347311621da3  syn-2.0.16.tar.gz
ba6faafb2dd56d694efe424752099a2efb50316afc0a4db9fdb7620ae3f1a31dfbb2a7b41724878cb977fa11f7568a406bd3b6a4f7cfc0b88b86b2cc616b953e  tempfile-3.3.0.tar.gz
cf1896523353390b2f90b2a8bf30f47da5fc7c2daa635bd0cd8059bdc73feb243e46e4279562fe45d5726f2840833b1e967c7de19ffc0c853592d9f86c0c1be7  termcolor-1.2.0.tar.gz
97ae8acece5663e1a6b08b827179e96d5ad0ee67d635888cc3d83454b52cf48fce97eb0eba374ba4747834099c74f43d66d9fec868e84be45369a42c1aaec2c3  textwrap-0.16.0.tar.gz
eddb82aeb8fdeb5436579292c6f7a64a90a2c7bb54070beb437bc7890b99795d0505faa8d6451a99e8bcf440f78db8a1b273a697c8ad44275cc4163a9ee49317  toml-0.5.11.tar.gz
8104999c6fff002c5aa109e2ca75ce3eaf772155d31dff87bcf39e3eb3da58b6cb543717be7b55acdb0cb1a4bd2a3d2e9c9974f7f75b6528668f5ef665ef4088  unicode-ident-1.0.8.tar.gz
ff8b7b78065f3d8999ec03c725a0460ebc059771bf071c7a3df3f0ecd733edf3b0a2450024d4e24e1aedddaecd9038ce1376c0d8bbf45132068cf45cf4a53a97  winapi-0.3.9.tar.gz
a672ccefd0730a8166fef1d4e39f9034d9ae426a3f5e28d1f4169fa5c5790767693f281d890e7804773b34acdb0ae1febac33cde8c50c0044a5a6152c7209ec2  winapi-i686-pc-windows-gnu-0.4.0.tar.gz
7baeb661f397c4693dfa001fdc774b323c51a7c55caad40f2de5112a1cefd1d6151e3df41fa4ee193460a5905917c83d2b1de5fa10b4bd014ad96690af95c0fd  winapi-util-0.1.5.tar.gz
4a654af6a5d649dc87e00497245096b35a2894ae66f155cb62389902c3b93ddcc5cf7d0d8b9dd97b291d2d80bc686af2298e80abef6ac69883f4a54e79712513  winapi-x86_64-pc-windows-gnu-0.4.0.tar.gz
299fbec38c0874bd53987ee8f8d793ac2f7b7695af22fb85b01dd0f3a20190d6920cc2deacc2a9da35bd58fd8eaab177aaa3460ef9e3f0897cd80d0c180e3ce0  ppc-libc-hugetlb.patch"
