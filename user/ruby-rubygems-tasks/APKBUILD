# Contributor: Dan Theisen <djt@hxx.in>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ruby-rubygems-tasks
_gemname=${pkgname#ruby-}
pkgver=0.2.5
pkgrel=0
pkgdesc="Provides agnostic and unobtrusive Rake tasks for maintaining Ruby Gems"
url="https://github.com/postmodern/rubygems-tasks"
arch="all"
options="!dbg"
license="MIT"
depends="ruby ruby-irb"
checkdepends="ruby-rspec"
makedepends="ruby-rake"
source="ruby-reline-$pkgver.tar.gz::https://github.com/postmodern/$_gemname/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	gem build $_gemname.gemspec
}

check() {
	rspec spec
}

package() {
	gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"
	
	gem install --local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname
	
	# Remove unnecessary files and empty directories.
	cd "$gemdir"
	rm -r cache build_info doc
}

sha512sums="518c1a4ccf3720ac16278359ebacf25fee298989ccc672bffc94119b005cbaffe668652c5081de3e6e0c321bfeeca61c6b93ba5d0741d5fa9a8ced34f5e5e889  ruby-reline-0.2.5.tar.gz"
