# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=libkworkspace
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE Plasma 5 workspace library"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test requires X11 accelration.
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0+ AND MIT AND LGPL-2.1-only AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
depends="kinit"
depends_dev="qt5-qtbase-dev"
makedepends="cmake extra-cmake-modules libice-dev libsm-dev libxau-dev
	kconfig-dev kcoreaddons-dev ki18n-dev kinit-dev kscreenlocker-dev
	kwin-dev kwindowsystem-dev plasma-framework-dev qt5-qtx11extras-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	session.patch
	standalone.patch
	"
builddir="$srcdir"/plasma-workspace-$pkgver/libkworkspace

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} . \
		-Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="ae40fe545b03e42e9f8fe2e8885d3853f4b52d4fd2ed3ecf7d24fa59b4924b8f8b389e8713ec6a3a875bc66df3952de9157d4a5631b283105a9329a1a1825996  plasma-workspace-5.24.5.tar.xz
30e7e39aa99a43c59fe7a88d389ba8309c66907bb55221d936c6a01e150f28e55b81a38dedb8dea3d73169581a29c9393b8fac0b5c7fc1e779d0b970da419d28  session.patch
867539bf129a28d06ba4838a716ec511980bb6b10f339180175855d1e3e933f4f2391fe3d9df7ea9b2cdb965c47d6746a12804acd98d5ed0d1979bf9630d4e8b  standalone.patch"
