# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=modemmanager-qt
pkgver=5.94.0
pkgrel=0
pkgdesc="Qt framework for ModemManager"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires MM running.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="modemmanager-dev"
makedepends="$depends_dev cmake doxygen extra-cmake-modules qt5-qtbase-dev
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/modemmanager-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3867d8ed2b000e51a2782ad0300e2a49b89bad429409b4d36f9d1798f992980b4dc6c38edceb8e33456bb5fb0d11b3007f01ca8c3bbaae6f239b0260885fdf9e  modemmanager-qt-5.94.0.tar.xz"
