# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=mbedtls
pkgver=3.6.0
pkgrel=0
pkgdesc="Lightweight TLS library"
url="https://tls.mbed.org"
arch="all"
license="Apache-2.0"
depends=""
checkdepends="python3"
makedepends="cmake"
subpackages="$pkgname-dev"
source="https://github.com/ARMmbed/mbedtls/releases/download/v$pkgver/$pkgname-$pkgver.tar.bz2"

# secfixes:
#   3.3.0-r0:
#     - CVE-2021-45451
#     - CVE-2022-35409
#     - CVE-2022-46392
#     - CVE-2022-46393

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	sed -i include/mbedtls/mbedtls_config.h \
		-e 's@^//#define MBEDTLS_SSL_DTLS_SRTP$@#define MBEDTLS_SSL_DTLS_SRTP@g' `#991` \
		;

	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -fPIC" \
		-DCMAKE_C_FLAGS="$CFLAGS -fPIC" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5c395890d486144af882aa96c9430103f79df889139969a64a490c0cafbdd5631a72bb668aa59b062204f19a5bb36dfcc5a096b59d76c7ef27f2560cd2388682  mbedtls-3.6.0.tar.bz2"
