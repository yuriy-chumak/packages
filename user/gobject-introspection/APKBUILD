# Maintainer: Zach van Rijn <me@zv.io>
pkgname=gobject-introspection
pkgver=1.72.0
pkgrel=0
pkgdesc="Introspection system for GObject-based libraries"
url="https://wiki.gnome.org/action/show/Projects/GObjectIntrospection"
arch="all"
license="LGPL-2.0+ AND GPL-2.0+ AND MIT"
depends=""
depends_dev="python3 cairo-dev libtool"
makedepends="$depends_dev bison flex glib-dev libffi-dev meson ninja python3-dev
	cmd:which"
checkdepends="sed"
subpackages="$pkgname-doc $pkgname-dev"
replaces="libgirepository"
source="https://download.gnome.org/sources/$pkgname/${pkgver%.*}/$pkgname-$pkgver.tar.xz
	musl-time64.patch
	support-cross-filesystem-move.patch
	"

build() {
	meson -Dprefix=/usr build
	ninja -C build
}

check() {
	ninja -C build test -j1
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

dev() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/share "$subpkgdir"/usr/
	mv "$pkgdir"/usr/lib/gobject-introspection "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
	default_dev
	replaces="gobject-introspection"
}

sha512sums="b8fba2bd12e93776c55228acf3487bef36ee40b1abdc7f681b827780ac94a8bfa1f59b0c30d60fa5a1fea2f610de78b9e52029f411128067808f17eb6374cdc5  gobject-introspection-1.72.0.tar.xz
784acecd0e139685664ba1af6f1df69aeeb2da11a2b7ef9b42f46f42699e087aa90f0a2d95c1ff7aa1c04a3f2505d6006015b5a0f1ba1b125d6b1311474736c2  musl-time64.patch
ce07017598d55a7a0b2a0cad18664a004d4f0425b57c29fd7d6c1d0d340e82db83c2a876cd60f3fb0fbc9e79d83badea4fc7c547f854d44b51bb980df8afa95a  support-cross-filesystem-move.patch"
