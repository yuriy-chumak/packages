From 29098b3d6718822fe41c6399cc7604422c8efd0c Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Fri, 8 Sep 2017 22:11:14 -0500
Subject: [PATCH 05/14] Remove musl_root and CRT fallback from musl targets

---
 compiler/rustc_codegen_ssa/src/back/link.rs   |  6 +--
 compiler/rustc_target/src/spec/crt_objects.rs | 25 -----------
 .../rustc_target/src/spec/linux_musl_base.rs  |  4 --
 config.toml.example                           | 17 --------
 src/bootstrap/cc_detect.rs                    | 27 +-----------
 src/bootstrap/compile.rs                      | 42 +------------------
 src/bootstrap/config.rs                       | 10 -----
 src/bootstrap/configure.py                    | 28 -------------
 src/bootstrap/lib.rs                          | 19 ---------
 src/bootstrap/sanity.rs                       | 23 ----------
 .../dist-i586-gnu-i586-i686-musl/Dockerfile   |  2 -
 .../host-x86_64/dist-various-1/Dockerfile     |  8 ----
 .../host-x86_64/dist-various-2/Dockerfile     |  3 +-
 .../host-x86_64/dist-x86_64-musl/Dockerfile   |  1 -
 .../host-x86_64/test-various/Dockerfile       |  1 -
 15 files changed, 5 insertions(+), 211 deletions(-)

diff --git a/compiler/rustc_codegen_ssa/src/back/link.rs b/compiler/rustc_codegen_ssa/src/back/link.rs
index 8f9eb619999f5..2c1c462adbf9f 100644
--- a/compiler/rustc_codegen_ssa/src/back/link.rs
+++ b/compiler/rustc_codegen_ssa/src/back/link.rs
@@ -1613,7 +1613,7 @@ fn detect_self_contained_mingw(sess: &Session) -> bool {
 /// Various toolchain components used during linking are used from rustc distribution
 /// instead of being found somewhere on the host system.
 /// We only provide such support for a very limited number of targets.
-fn self_contained(sess: &Session, crate_type: CrateType) -> bool {
+fn self_contained(sess: &Session, _crate_type: CrateType) -> bool {
     if let Some(self_contained) = sess.opts.cg.link_self_contained {
         return self_contained;
     }
@@ -1621,10 +1621,6 @@ fn self_contained(sess: &Session, crate_type: CrateType) -> bool {
     match sess.target.link_self_contained {
         LinkSelfContainedDefault::False => false,
         LinkSelfContainedDefault::True => true,
-        // FIXME: Find a better heuristic for "native musl toolchain is available",
-        // based on host and linker path, for example.
-        // (https://github.com/rust-lang/rust/pull/71769#issuecomment-626330237).
-        LinkSelfContainedDefault::Musl => sess.crt_static(Some(crate_type)),
         LinkSelfContainedDefault::Mingw => {
             sess.host == sess.target
                 && sess.target.vendor != "uwp"
diff --git a/compiler/rustc_target/src/spec/crt_objects.rs b/compiler/rustc_target/src/spec/crt_objects.rs
index c126390f5a908..63e3074b5b7c2 100644
--- a/compiler/rustc_target/src/spec/crt_objects.rs
+++ b/compiler/rustc_target/src/spec/crt_objects.rs
@@ -63,28 +63,6 @@ pub(super) fn all(obj: &'static str) -> CrtObjects {
     ])
 }
 
-pub(super) fn pre_musl_self_contained() -> CrtObjects {
-    new(&[
-        (LinkOutputKind::DynamicNoPicExe, &["crt1.o", "crti.o", "crtbegin.o"]),
-        (LinkOutputKind::DynamicPicExe, &["Scrt1.o", "crti.o", "crtbeginS.o"]),
-        (LinkOutputKind::StaticNoPicExe, &["crt1.o", "crti.o", "crtbegin.o"]),
-        (LinkOutputKind::StaticPicExe, &["rcrt1.o", "crti.o", "crtbeginS.o"]),
-        (LinkOutputKind::DynamicDylib, &["crti.o", "crtbeginS.o"]),
-        (LinkOutputKind::StaticDylib, &["crti.o", "crtbeginS.o"]),
-    ])
-}
-
-pub(super) fn post_musl_self_contained() -> CrtObjects {
-    new(&[
-        (LinkOutputKind::DynamicNoPicExe, &["crtend.o", "crtn.o"]),
-        (LinkOutputKind::DynamicPicExe, &["crtendS.o", "crtn.o"]),
-        (LinkOutputKind::StaticNoPicExe, &["crtend.o", "crtn.o"]),
-        (LinkOutputKind::StaticPicExe, &["crtendS.o", "crtn.o"]),
-        (LinkOutputKind::DynamicDylib, &["crtendS.o", "crtn.o"]),
-        (LinkOutputKind::StaticDylib, &["crtendS.o", "crtn.o"]),
-    ])
-}
-
 pub(super) fn pre_mingw_self_contained() -> CrtObjects {
     new(&[
         (LinkOutputKind::DynamicNoPicExe, &["crt2.o", "rsbegin.o"]),
@@ -130,7 +108,6 @@ pub(super) fn post_wasi_self_contained() -> CrtObjects {
 pub enum LinkSelfContainedDefault {
     False,
     True,
-    Musl,
     Mingw,
 }
 
@@ -141,7 +118,6 @@ impl FromStr for LinkSelfContainedDefault {
         Ok(match s {
             "false" => LinkSelfContainedDefault::False,
             "true" | "wasm" => LinkSelfContainedDefault::True,
-            "musl" => LinkSelfContainedDefault::Musl,
             "mingw" => LinkSelfContainedDefault::Mingw,
             _ => return Err(()),
         })
@@ -153,7 +129,6 @@ impl ToJson for LinkSelfContainedDefault {
         match *self {
             LinkSelfContainedDefault::False => "false",
             LinkSelfContainedDefault::True => "true",
-            LinkSelfContainedDefault::Musl => "musl",
             LinkSelfContainedDefault::Mingw => "mingw",
         }
         .to_json()
diff --git a/compiler/rustc_target/src/spec/linux_musl_base.rs b/compiler/rustc_target/src/spec/linux_musl_base.rs
index 61553e71b4500..67d18c886eba5 100644
--- a/compiler/rustc_target/src/spec/linux_musl_base.rs
+++ b/compiler/rustc_target/src/spec/linux_musl_base.rs
@@ -1,13 +1,9 @@
-use crate::spec::crt_objects::{self, LinkSelfContainedDefault};
 use crate::spec::TargetOptions;
 
 pub fn opts() -> TargetOptions {
     let mut base = super::linux_base::opts();
 
     base.env = "musl".into();
-    base.pre_link_objects_self_contained = crt_objects::pre_musl_self_contained();
-    base.post_link_objects_self_contained = crt_objects::post_musl_self_contained();
-    base.link_self_contained = LinkSelfContainedDefault::Musl;
 
     // These targets statically link libc by default
     base.crt_static_default = true;
diff --git a/config.toml.example b/config.toml.example
index a46813e4d7a3b..377d9b07ceb20 100644
--- a/config.toml.example
+++ b/config.toml.example
@@ -533,14 +533,6 @@ changelog-seen = 2
 # behavior -- this may lead to miscompilations or other bugs.
 #description = <none> (string)
 
-# The root location of the musl installation directory. The library directory
-# will also need to contain libunwind.a for an unwinding implementation. Note
-# that this option only makes sense for musl targets that produce statically
-# linked binaries.
-#
-# Defaults to /usr on musl hosts. Has no default otherwise.
-#musl-root = <platform specific> (path)
-
 # By default the `rustc` executable is built with `-Wl,-rpath` flags on Unix
 # platforms to ensure that the compiler is usable by default from the build
 # directory (as it links to a number of dynamic libraries). This may not be
@@ -722,15 +714,6 @@ changelog-seen = 2
 # only use static libraries. If unset, the target's default linkage is used.
 #crt-static = <platform-specific> (bool)
 
-# The root location of the musl installation directory. The library directory
-# will also need to contain libunwind.a for an unwinding implementation. Note
-# that this option only makes sense for musl targets that produce statically
-# linked binaries.
-#musl-root = build.musl-root (path)
-
-# The full path to the musl libdir.
-#musl-libdir = musl-root/lib
-
 # The root location of the `wasm32-wasi` sysroot. Only used for the
 # `wasm32-wasi` target. If you are building wasm32-wasi target, make sure to
 # create a `[target.wasm32-wasi]` section and move this field there.
diff --git a/src/bootstrap/cc_detect.rs b/src/bootstrap/cc_detect.rs
index 759a99c330c27..6f48fd8efc1b9 100644
--- a/src/bootstrap/cc_detect.rs
+++ b/src/bootstrap/cc_detect.rs
@@ -41,8 +41,6 @@ fn cc2ar(cc: &Path, target: TargetSelection) -> Option<PathBuf> {
         Some(PathBuf::from(ar))
     } else if target.contains("msvc") {
         None
-    } else if target.contains("musl") {
-        Some(PathBuf::from("ar"))
     } else if target.contains("openbsd") {
         Some(PathBuf::from("ar"))
     } else if target.contains("vxworks") {
@@ -101,7 +99,7 @@ pub fn find(build: &mut Build) {
         if let Some(cc) = config.and_then(|c| c.cc.as_ref()) {
             cfg.compiler(cc);
         } else {
-            set_compiler(&mut cfg, Language::C, target, config, build);
+            set_compiler(&mut cfg, Language::C, target, config);
         }
 
         let compiler = cfg.get_compiler();
@@ -122,7 +120,7 @@ pub fn find(build: &mut Build) {
             cfg.compiler(cxx);
             true
         } else if build.hosts.contains(&target) || build.build == target {
-            set_compiler(&mut cfg, Language::CPlusPlus, target, config, build);
+            set_compiler(&mut cfg, Language::CPlusPlus, target, config);
             true
         } else {
             // Use an auto-detected compiler (or one configured via `CXX_target_triple` env vars).
@@ -158,7 +156,6 @@ fn set_compiler(
     compiler: Language,
     target: TargetSelection,
     config: Option<&Target>,
-    build: &Build,
 ) {
     match &*target.triple {
         // When compiling for android we may have the NDK configured in the
@@ -201,26 +198,6 @@ fn set_compiler(
             }
         }
 
-        "mips-unknown-linux-musl" => {
-            if cfg.get_compiler().path().to_str() == Some("gcc") {
-                cfg.compiler("mips-linux-musl-gcc");
-            }
-        }
-        "mipsel-unknown-linux-musl" => {
-            if cfg.get_compiler().path().to_str() == Some("gcc") {
-                cfg.compiler("mipsel-linux-musl-gcc");
-            }
-        }
-
-        t if t.contains("musl") => {
-            if let Some(root) = build.musl_root(target) {
-                let guess = root.join("bin/musl-gcc");
-                if guess.exists() {
-                    cfg.compiler(guess);
-                }
-            }
-        }
-
         _ => {}
     }
 }
diff --git a/src/bootstrap/compile.rs b/src/bootstrap/compile.rs
index e02a10b816408..a5bc42e797bf7 100644
--- a/src/bootstrap/compile.rs
+++ b/src/bootstrap/compile.rs
@@ -230,38 +230,7 @@ fn copy_self_contained_objects(
 
     // Copies the libc and CRT objects.
     //
-    // rustc historically provides a more self-contained installation for musl targets
-    // not requiring the presence of a native musl toolchain. For example, it can fall back
-    // to using gcc from a glibc-targeting toolchain for linking.
-    // To do that we have to distribute musl startup objects as a part of Rust toolchain
-    // and link with them manually in the self-contained mode.
-    if target.contains("musl") {
-        let srcdir = builder.musl_libdir(target).unwrap_or_else(|| {
-            panic!("Target {:?} does not have a \"musl-libdir\" key", target.triple)
-        });
-        for &obj in &["libc.a", "crt1.o", "Scrt1.o", "rcrt1.o", "crti.o", "crtn.o"] {
-            copy_and_stamp(
-                builder,
-                &libdir_self_contained,
-                &srcdir,
-                obj,
-                &mut target_deps,
-                DependencyType::TargetSelfContained,
-            );
-        }
-        let crt_path = builder.ensure(native::CrtBeginEnd { target });
-        for &obj in &["crtbegin.o", "crtbeginS.o", "crtend.o", "crtendS.o"] {
-            let src = crt_path.join(obj);
-            let target = libdir_self_contained.join(obj);
-            builder.copy(&src, &target);
-            target_deps.push((target, DependencyType::TargetSelfContained));
-        }
-
-        if !target.starts_with("s390x") {
-            let libunwind_path = copy_llvm_libunwind(builder, target, &libdir_self_contained);
-            target_deps.push((libunwind_path, DependencyType::TargetSelfContained));
-        }
-    } else if target.ends_with("-wasi") {
+    if target.ends_with("-wasi") {
         let srcdir = builder
             .wasi_root(target)
             .unwrap_or_else(|| {
@@ -350,15 +319,6 @@ pub fn std_cargo(builder: &Builder<'_>, target: TargetSelection, stage: u32, car
             .arg("--manifest-path")
             .arg(builder.src.join("library/test/Cargo.toml"));
 
-        // Help the libc crate compile by assisting it in finding various
-        // sysroot native libraries.
-        if target.contains("musl") {
-            if let Some(p) = builder.musl_libdir(target) {
-                let root = format!("native={}", p.to_str().unwrap());
-                cargo.rustflag("-L").rustflag(&root);
-            }
-        }
-
         if target.ends_with("-wasi") {
             if let Some(p) = builder.wasi_root(target) {
                 let root = format!("native={}/lib/wasm32-wasi", p.to_str().unwrap());
diff --git a/src/bootstrap/config.rs b/src/bootstrap/config.rs
index a8c403675d821..d1637104ff129 100644
--- a/src/bootstrap/config.rs
+++ b/src/bootstrap/config.rs
@@ -190,8 +190,6 @@ pub struct Config {
     pub print_step_rusage: bool,
     pub missing_tools: bool,
 
-    // Fallback musl-root for all targets
-    pub musl_root: Option<PathBuf>,
     pub prefix: Option<PathBuf>,
     pub sysconfdir: Option<PathBuf>,
     pub datadir: Option<PathBuf>,
@@ -429,8 +427,6 @@ pub struct Target {
     pub sanitizers: Option<bool>,
     pub profiler: Option<bool>,
     pub crt_static: Option<bool>,
-    pub musl_root: Option<PathBuf>,
-    pub musl_libdir: Option<PathBuf>,
     pub wasi_root: Option<PathBuf>,
     pub qemu_rootfs: Option<PathBuf>,
     pub no_std: bool,
@@ -723,7 +719,6 @@ define_config! {
         default_linker: Option<String> = "default-linker",
         channel: Option<String> = "channel",
         description: Option<String> = "description",
-        musl_root: Option<String> = "musl-root",
         rpath: Option<bool> = "rpath",
         verbose_tests: Option<bool> = "verbose-tests",
         optimize_tests: Option<bool> = "optimize-tests",
@@ -770,8 +765,6 @@ define_config! {
         sanitizers: Option<bool> = "sanitizers",
         profiler: Option<bool> = "profiler",
         crt_static: Option<bool> = "crt-static",
-        musl_root: Option<String> = "musl-root",
-        musl_libdir: Option<String> = "musl-libdir",
         wasi_root: Option<String> = "wasi-root",
         qemu_rootfs: Option<String> = "qemu-rootfs",
         no_std: Option<bool> = "no-std",
@@ -1175,7 +1168,6 @@ impl Config {
             set(&mut config.llvm_tools_enabled, rust.llvm_tools);
             config.rustc_parallel = rust.parallel_compiler.unwrap_or(false);
             config.rustc_default_linker = rust.default_linker;
-            config.musl_root = rust.musl_root.map(PathBuf::from);
             config.save_toolstates = rust.save_toolstates.map(PathBuf::from);
             set(&mut config.deny_warnings, flags.deny_warnings.or(rust.deny_warnings));
             set(&mut config.backtrace_on_ice, rust.backtrace_on_ice);
@@ -1235,8 +1227,6 @@ impl Config {
                 target.ranlib = cfg.ranlib.map(PathBuf::from);
                 target.linker = cfg.linker.map(PathBuf::from);
                 target.crt_static = cfg.crt_static;
-                target.musl_root = cfg.musl_root.map(PathBuf::from);
-                target.musl_libdir = cfg.musl_libdir.map(PathBuf::from);
                 target.wasi_root = cfg.wasi_root.map(PathBuf::from);
                 target.qemu_rootfs = cfg.qemu_rootfs.map(PathBuf::from);
                 target.sanitizers = cfg.sanitizers;
diff --git a/src/bootstrap/configure.py b/src/bootstrap/configure.py
index 6b139decb5551..a2e19b8f4836d 100755
--- a/src/bootstrap/configure.py
+++ b/src/bootstrap/configure.py
@@ -112,34 +112,6 @@ def v(*args):
   "aarch64-linux-android NDK standalone path")
 v("x86_64-linux-android-ndk", "target.x86_64-linux-android.android-ndk",
   "x86_64-linux-android NDK standalone path")
-v("musl-root", "target.x86_64-unknown-linux-musl.musl-root",
-  "MUSL root installation directory (deprecated)")
-v("musl-root-x86_64", "target.x86_64-unknown-linux-musl.musl-root",
-  "x86_64-unknown-linux-musl install directory")
-v("musl-root-i586", "target.i586-unknown-linux-musl.musl-root",
-  "i586-unknown-linux-musl install directory")
-v("musl-root-i686", "target.i686-unknown-linux-musl.musl-root",
-  "i686-unknown-linux-musl install directory")
-v("musl-root-arm", "target.arm-unknown-linux-musleabi.musl-root",
-  "arm-unknown-linux-musleabi install directory")
-v("musl-root-armhf", "target.arm-unknown-linux-musleabihf.musl-root",
-  "arm-unknown-linux-musleabihf install directory")
-v("musl-root-armv5te", "target.armv5te-unknown-linux-musleabi.musl-root",
-  "armv5te-unknown-linux-musleabi install directory")
-v("musl-root-armv7", "target.armv7-unknown-linux-musleabi.musl-root",
-  "armv7-unknown-linux-musleabi install directory")
-v("musl-root-armv7hf", "target.armv7-unknown-linux-musleabihf.musl-root",
-  "armv7-unknown-linux-musleabihf install directory")
-v("musl-root-aarch64", "target.aarch64-unknown-linux-musl.musl-root",
-  "aarch64-unknown-linux-musl install directory")
-v("musl-root-mips", "target.mips-unknown-linux-musl.musl-root",
-  "mips-unknown-linux-musl install directory")
-v("musl-root-mipsel", "target.mipsel-unknown-linux-musl.musl-root",
-  "mipsel-unknown-linux-musl install directory")
-v("musl-root-mips64", "target.mips64-unknown-linux-muslabi64.musl-root",
-  "mips64-unknown-linux-muslabi64 install directory")
-v("musl-root-mips64el", "target.mips64el-unknown-linux-muslabi64.musl-root",
-  "mips64el-unknown-linux-muslabi64 install directory")
 v("qemu-armhf-rootfs", "target.arm-unknown-linux-gnueabihf.qemu-rootfs",
   "rootfs in qemu testing, you probably don't want to use this")
 v("qemu-aarch64-rootfs", "target.aarch64-unknown-linux-gnu.qemu-rootfs",
diff --git a/src/bootstrap/lib.rs b/src/bootstrap/lib.rs
index 65a2e022ef5f6..08dc576bc2091 100644
--- a/src/bootstrap/lib.rs
+++ b/src/bootstrap/lib.rs
@@ -1168,25 +1168,6 @@ impl Build {
         }
     }
 
-    /// Returns the "musl root" for this `target`, if defined
-    fn musl_root(&self, target: TargetSelection) -> Option<&Path> {
-        self.config
-            .target_config
-            .get(&target)
-            .and_then(|t| t.musl_root.as_ref())
-            .or_else(|| self.config.musl_root.as_ref())
-            .map(|p| &**p)
-    }
-
-    /// Returns the "musl libdir" for this `target`.
-    fn musl_libdir(&self, target: TargetSelection) -> Option<PathBuf> {
-        let t = self.config.target_config.get(&target)?;
-        if let libdir @ Some(_) = &t.musl_libdir {
-            return libdir.clone();
-        }
-        self.musl_root(target).map(|root| root.join("lib"))
-    }
-
     /// Returns the sysroot for the wasi target, if defined
     fn wasi_root(&self, target: TargetSelection) -> Option<&Path> {
         self.config.target_config.get(&target).and_then(|t| t.wasi_root.as_ref()).map(|p| &**p)
diff --git a/src/bootstrap/sanity.rs b/src/bootstrap/sanity.rs
index e905517253c0a..ba1ca3c119546 100644
--- a/src/bootstrap/sanity.rs
+++ b/src/bootstrap/sanity.rs
@@ -11,7 +11,6 @@
 use std::collections::HashMap;
 use std::env;
 use std::ffi::{OsStr, OsString};
-use std::fs;
 use std::path::PathBuf;
 use std::process::Command;
 
@@ -190,28 +189,6 @@ than building it.
             }
         }
 
-        // Make sure musl-root is valid
-        if target.contains("musl") {
-            // If this is a native target (host is also musl) and no musl-root is given,
-            // fall back to the system toolchain in /usr before giving up
-            if build.musl_root(*target).is_none() && build.config.build == *target {
-                let target = build.config.target_config.entry(*target).or_default();
-                target.musl_root = Some("/usr".into());
-            }
-            match build.musl_libdir(*target) {
-                Some(libdir) => {
-                    if fs::metadata(libdir.join("libc.a")).is_err() {
-                        panic!("couldn't find libc.a in musl libdir: {}", libdir.display());
-                    }
-                }
-                None => panic!(
-                    "when targeting MUSL either the rust.musl-root \
-                            option or the target.$TARGET.musl-root option must \
-                            be specified in config.toml"
-                ),
-            }
-        }
-
         if need_cmake && target.contains("msvc") {
             // There are three builds of cmake on windows: MSVC, MinGW, and
             // Cygwin. The Cygwin build does not have generators for Visual
diff --git a/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile b/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile
index 26eb69f2eae93..8b2d087e63a8d 100644
--- a/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile
@@ -58,8 +58,6 @@ COPY scripts/sccache.sh /scripts/
 RUN sh /scripts/sccache.sh
 
 ENV RUST_CONFIGURE_ARGS \
-      --musl-root-i586=/musl-i586 \
-      --musl-root-i686=/musl-i686 \
       --disable-docs
 
 # Newer binutils broke things on some vms/distros (i.e., linking against
diff --git a/src/ci/docker/host-x86_64/dist-various-1/Dockerfile b/src/ci/docker/host-x86_64/dist-various-1/Dockerfile
index cd0f01faa1bfa..6951c9ea9b1b3 100644
--- a/src/ci/docker/host-x86_64/dist-various-1/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-various-1/Dockerfile
@@ -172,14 +172,6 @@ ENV CFLAGS_armv5te_unknown_linux_musleabi="-march=armv5te -marm -mfloat-abi=soft
     CC_riscv64gc_unknown_none_elf=false
 
 ENV RUST_CONFIGURE_ARGS \
-      --musl-root-armv5te=/musl-armv5te \
-      --musl-root-arm=/musl-arm \
-      --musl-root-armhf=/musl-armhf \
-      --musl-root-armv7hf=/musl-armv7hf \
-      --musl-root-mips=/musl-mips \
-      --musl-root-mipsel=/musl-mipsel \
-      --musl-root-mips64=/musl-mips64 \
-      --musl-root-mips64el=/musl-mips64el \
       --disable-docs
 
 ENV SCRIPT \
diff --git a/src/ci/docker/host-x86_64/dist-various-2/Dockerfile b/src/ci/docker/host-x86_64/dist-various-2/Dockerfile
index 8250ec0c3119b..204048f2db149 100644
--- a/src/ci/docker/host-x86_64/dist-various-2/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-various-2/Dockerfile
@@ -126,8 +126,7 @@ ENV TARGETS=$TARGETS,x86_64-unknown-none
 RUN ln -s /usr/include/asm-generic /usr/local/include/asm
 
 ENV RUST_CONFIGURE_ARGS --enable-extended --enable-lld --disable-docs \
-  --set target.wasm32-wasi.wasi-root=/wasm32-wasi \
-  --musl-root-armv7=/musl-armv7
+  --set target.wasm32-wasi.wasi-root=/wasm32-wasi
 
 ENV EXTERNAL_LLVM 1
 
diff --git a/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile b/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile
index 51645a81853fd..b9edb339cc0e5 100644
--- a/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile
@@ -37,7 +37,6 @@ RUN sh /scripts/sccache.sh
 ENV HOSTS=x86_64-unknown-linux-musl
 
 ENV RUST_CONFIGURE_ARGS \
-      --musl-root-x86_64=/usr/local/x86_64-linux-musl \
       --enable-extended \
       --enable-sanitizers \
       --enable-profiler \
diff --git a/src/ci/docker/host-x86_64/test-various/Dockerfile b/src/ci/docker/host-x86_64/test-various/Dockerfile
index b75e2f085cd3b..a9022b52d3324 100644
--- a/src/ci/docker/host-x86_64/test-various/Dockerfile
+++ b/src/ci/docker/host-x86_64/test-various/Dockerfile
@@ -35,7 +35,6 @@ RUN sh /scripts/sccache.sh
 ENV NO_DOWNLOAD_CI_LLVM 1
 
 ENV RUST_CONFIGURE_ARGS \
-  --musl-root-x86_64=/usr/local/x86_64-linux-musl \
   --set build.nodejs=/node-v15.14.0-linux-x64/bin/node \
   --set rust.lld
 
