# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.68.2
_bootver=1.67.1-r0
_llvmver=14
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0007-test-failed-doctest-output-Fix-normalization.patch
	0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0009-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0010-Ignore-broken-and-non-applicable-tests.patch
	0011-Link-stage-2-tools-dynamically-to-libstd.patch
	0012-Move-debugger-scripts-to-usr-share-rust.patch
	0013-Add-foxkit-target-specs.patch
	powerpc-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 2
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast || true
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="8b085d0351e19100e9abc24b10c44a0939a1d35ba23421da4ece345d7373f7dbad1dc6a2ae153c1259404dd96b41e2682e711cf2b0b63fd03a196760cddbcdd3  rustc-1.68.2-src.tar.xz
2eab0513b3fbe27e635de257f931f85f0e275362b6bbeca296e582b39b4bf6b9f4bb7551c1a8ea2aebb3240df98fbcd727e384506d4059215a66335411ec9168  0001-Fix-LLVM-build.patch
4388c90bc285db5689e31b81700df13e868703fa76613afe96858c0ca0bb8ab1b569c3b6580bdc0f7bb6bdb7201c901d78e86547458aa8700bd7fa5ebeff0453  0002-Fix-linking-to-zlib-when-cross-compiling.patch
887babf6ca5cb969fdf91e5930fa966aa7979246a4031c749233997f919df881f198ad8a22e8b260ee841b52ba46de83f080b99bdf2a44ba864854cf3bfdd437  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
a284464636b86f62797473db5ee5b3035a210ea96bdde31a6eac014f4eee03ae2580a91b8fd95697315581c545959087f6647da302f8df1d06a970caf4be52ba  0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
33c1d5b2f051f4ccfde4cdd9cfed4a97b23ccc50aac2eed300b989fff8e1ad55e0ab140c128d33fca25b5b54cfb33b948bc6a139f93d232661e07bf4eba1dc24  0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
3cf4663242bfd6c67a644a2c6e9c49d26fb2dc93b8907daa86880adb9f57a3db222121a68004aae3f048da59d123ace301e28c63ea80a80f61f4b63f8089e497  0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
f0886d31ea28f37359ade3a201f56e48d7bd0d12f5d3e0aeac6044acf0d3bf27a18788ddf31c4ac1243acfe2e1f9e964de9a328997449f6f0eb4ac2fd5e4f316  0007-test-failed-doctest-output-Fix-normalization.patch
85699f22dce83a270885b4709a53ff9cd4495260d77fdbbb02e90efd164aaa9d023bd32e0ed6297b37ada76dcb51b432299438143acd24d215185ed2cd06576f  0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
9b878e77c2a15d5528be7d899b69f5fcf57624d1463d701a4ff01dad239201929a39cecbe4d653879a305f25b17a0761231c755803cdcf253338c5d535633ba4  0009-test-use-extern-for-plugins-Don-t-assume-multilib.patch
586c7bade5c24209515a29b810262084d13ae854f1c5832ffc47eab5d765cf17d9e327ca0643d4a85b51c7daf7cccc66b5279441702bbd351b397be012b3ea5b  0010-Ignore-broken-and-non-applicable-tests.patch
e5e7c1179880230c5f77236c0d04bfa6a41c138794992c4a20ad86e81311df1ddedaaaf101f0a31d83d2a439ee8c2696c4f07b9210baa6105ef69c77fe564dfa  0011-Link-stage-2-tools-dynamically-to-libstd.patch
cc87d6a02985a2e0e540cba57a35c42ad559fd34820c5ac32fe8d0ce946975f4225d20b7947383bb9cd718e8f8110fcb842f33c756a532a09025d9df7c742ebf  0012-Move-debugger-scripts-to-usr-share-rust.patch
1120672d2fa765c3c2369b9b0cbb6c7d1bc1354bd4a090ebcde4cfd7abb9c32aa0267e83ec208e3e253a8ad59b2478ae079ece1839257d49324ebd7c7ab81613  0013-Add-foxkit-target-specs.patch
e2001f133dd465020a541c36fea034bbc17c9b3130433eb5dfa9f88fa2d539286c419a49a38261275cb03dd25e5977cb57af776c1dd80fdeea47813695eb4818  powerpc-atomics.patch"
